<!doctype html>

<html class="no-js" lang="en">
<head>


    <!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjHsSALqSFj8Wle0NSvx0YCggt01gP_iY"></script> -->


    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBxsN5sYLbpWksN3zCjoZYpGipMZi1gF4" type="text/javascript"></script>



    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri() ?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#29a7d6">
    <meta name="theme-color" content="#ffffff">
    <meta name="robots" content="index, follow">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script>document.documentElement.className = document.documentElement.className.replace("no-js","js");</script>
    <?php wp_head(); ?>




</head>

<body>




<div class="cookies" >
    <div class="cookies__card">
        <div class="cookies-top">
            <div class="cookies-top__logo" >

            </div>
            <div class="cookies-top__close" onclick="hideCookie()">
                <span class="cookies-close" ></span>
            </div>
        </div>
        <div class="cookies-section">
            <div class="cookies-title">Nastavení preferencí souborů cookies</div>
            <p class="cookies-text">Souhlasem nám umožníte využívat soubory cookies k poskytovaní služeb a analýze návštevnosti.</p>
            <p class="cookies-text">Soubory cookies zjednodušují používání webů. Ukládají předvolby uživatelů a pomáhají k zobrazení obsahu podle Vašich potřeb. Respektujeme Vaše soukromí, proto se můžete rozhodnout některé soubory cookies neakceptovat. Cookies slouží i k zajištění funkčnosti a bezpečnosti webových stránek. Zablokování některých z nich může ovlivnit funkčnost stránek či nabízených služeb.</p>
            <div class="cookies-button">
                <button class="btn btn__orange" onclick="cookiesAllowAll()">
                    Povolit vše
                </button>
            </div>
        </div>
        <div class="cookies-section">
            <div class="cookies-title">Řízení předvoleb poskytování souhlasu</div>
            <div class="cookies-items" >

            </div>

            <div class="cookies-button">
                <button class="btn btn__blue" style="    padding: 10px 15px;
    font-size: 14px;"  onclick="cookiesAllow()">
                    Povolit vybrané
                </button>
            </div>
            <div class="cookies-button">
                <button class="btn btn__orange"  onclick="cookiesAllowAll()">
                    Povolit vše
                </button>
            </div>
        </div>
    </div>
</div>

<div class="cookie" id="cookie"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>



        let cookies = [
            {
                type: 'normal',
                title: 'Naprosto nezbytné soubory cookie',
                text: 'Tyto soubory cookie neukládají žádné osobní identifikovatelné informace . Jsou nezbytné k tomu, aby web fungoval, takže není možné je vypnout. Většinou jsou nastavené jako odezva na akce, které jste provedli. Například požadavek služeb týkajících se bezpečnostních nastavení, přihlašování, vyplňování formulářů atp. Svůj prohlížeč můžete ale nastavit, aby soubory cookie blokoval nebo o nich posílal upozornění. Mějte na paměti, že některé stránky bez těchto souborů nebudou fungovat.',
                switcher: false,
                checked: true,
                opened:false,
            },
            {
                type: 'analytics',
                title: 'Analytické soubory cookie',
                text: 'Všechny informace, které soubory cookie shromažďují, jsou souhrnné. Pomáhají sledovat počet návštěvníků a také z jakého zdroje provoz pochází. To nám umožňuje zlepšovat výkon stránky. Můžeme s nimi určovat, které stránky jsou nejoblíbenější a které ne, a také sledovat, jakým způsobem se návštěvníci na webu pohybují. Pokud soubory cookie nepovolíte, nebudeme vědět, kdy jste navštívili naši stránku.',
                switcher: true,
                checked: false,
                opened:false,
            },
            /*       {
                     type: 'functions',
                     title: 'Funkční soubory cookie',
                     text: 'S těmito soubory cookie je stránka výkonnější a zaměřená osobněji. Pokud soubory cookie nepovolíte, je možné, že některé nebo všechny tyto služby nebudou fungovat správně.',
                     switcher: true,
                     checked: false,
                     opened:false,
                   },*/
            {
                type: 'marketing',
                title: 'Marketingové soubory cookie',
                text: 'Tyto soubory cookies nám pomáhají personalizovat reklamu, kterou vidíte na našich stránkách i stránkách našich partnerů. Na základě těchto cookies můžeme my i jiní poskytovatelé vytvářet profily o Vašich zájmech a podle nich Vám zobrazovat relevantní reklamy. Pokud je nepovolíte, bude se Vám na stránkách zobrazovat méně cílená reklama.',
                switcher: true,
                checked: false,
                opened:false,
            }
        ];


        function checkScripts(){
            var cookieItem = null;
            console.log('cookie-data-before');
            cookiesChecked.forEach(cookieChecked => {
                console.log('cookie-data-invalid');
                if(cookieChecked && cookieChecked.type === 'analytics' && cookieChecked.checked){
                    console.log('cookie-data-valid');
                    cookieItem = document.createElement('script')
                    cookieItem.type = "text/javascript"
                    cookieItem.setAttribute('async', true)
                    cookieItem.src = 'https://www.googletagmanager.com/gtag/js?id=UA-173349252-1';
                    document.head.appendChild(cookieItem)

                    cookieItem = document.createElement('script')
                    cookieItem.innerHTML = " window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-173349252-1')";
                    document.head.appendChild(cookieItem)

                }
            })
        }

        var cookiesChecked = JSON.parse(window.localStorage.getItem('cookies'));


        window.addEventListener('load', function() {
            console.log('pretest')
            openCookie();
            checkCookies();
        })







        function checkCookies(){

            console.log('test')
            var checkedSome = 0;
            cookiesChecked = JSON.parse(window.localStorage.getItem('cookies'));

            if(cookiesChecked){
                cookies.forEach(cookie => {
                    if(cookie.checked){
                        checkedSome++;
                    }
                    cookiesChecked.forEach(cookieChecked => {
                        if(cookieChecked && cookieChecked.type && cookie){
                            if(cookie.type === cookieChecked.type){
                                cookie.checked = cookieChecked.checked;
                            }
                        }
                    })
                })

                if(cookies.length === cookiesChecked.filter(cookieChecked => cookieChecked.checked).length){
                    $('.cookie').remove()
                    $('.cookies').removeClass('cookies--active');

                }else{
                    $('.cookie').addClass('cookie--active')
                }


                checkScripts()

            }else{
                $('.cookie').addClass('cookie--active')
            }






            generateSwitchers();


        }






        function generateSwitchers(){

            $('.cookies-items').html('');

            cookies.forEach(cookie => {

                console.log(cookie.checked)
                var cookieItem = null;



                if(cookie.switcher){
                    if(cookie.checked){
                        cookieItem = "<div class=\"cookies-item\" >\n" +
                            "                    <div class=\"cookies-item__tab\" >\n" +
                            "                        <div class=\"cookies-item-text\">\n" +
                            cookie.title +
                            "                        </div>\n" +
                            "                        <div class=\"cookies-item-switcher-always-active\" >\n" +
                            "                            <div class=\"switcher-wrap\">\n" +
                            "                                <div class=\"switcher\">\n" +
                            "                                    <div class=\"switcher__item switcher__item--toggled " +

                            cookie.type +
                            "\" >\n" +
                            "                                        <div class=\"switcher__item__button\">\n" +
                            "                                        </div>\n" +
                            "                                    </div>\n" +
                            "                                </div>\n" +
                            "                            </div>\n" +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                    <div class=\"cookies-item__data\" >\n" +
                            "                        <div class=\"cookies-text cookies-text--light\" >\n" +
                            cookie.text +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                </div>";
                    }else{
                        cookieItem = "<div class=\"cookies-item\" >\n" +
                            "                    <div class=\"cookies-item__tab\" >\n" +
                            "                        <div class=\"cookies-item-text\">\n" +
                            cookie.title +
                            "                        </div>\n" +
                            "                        <div class=\"cookies-item-switcher-always-active\" >\n" +
                            "                            <div class=\"switcher-wrap\">\n" +
                            "                                <div class=\"switcher\">\n" +
                            "                                    <div class=\"switcher__item " +

                            cookie.type +
                            "\" >\n" +
                            "                                        <div class=\"switcher__item__button\">\n" +
                            "                                        </div>\n" +
                            "                                    </div>\n" +
                            "                                </div>\n" +
                            "                            </div>\n" +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                    <div class=\"cookies-item__data\" >\n" +
                            "                        <div class=\"cookies-text cookies-text--light\" >\n" +
                            cookie.text +
                            "                        </div>\n" +
                            "                    </div>\n" +
                            "                </div>";
                    }

                }else{
                    cookieItem = "<div class=\"cookies-item\" >\n" +
                        "                    <div class=\"cookies-item__tab\" >\n" +
                        "                        <div class=\"cookies-item-text\">\n" +
                        cookie.title +
                        "                        </div>\n" +
                        "                        <div class=\"cookies-item-switcher-always-active\" >\n" +
                        "                            vždy aktivní\n" +
                        "                        </div>\n" +
                        "                    </div>\n" +
                        "                    <div class=\"cookies-item__data\" >\n" +
                        "                        <div class=\"cookies-text cookies-text--light\" >\n" +
                        cookie.text +
                        "                        </div>\n" +
                        "                    </div>\n" +
                        "                </div>";
                }


                $('.cookies-items').append(cookieItem);

            })

            $( ".cookies-item__tab" ).click(function(e) {
                console.log('test');

                if($(this).closest('.cookies-item').hasClass( "cookies-item--active" )){
                    $(this).closest('.cookies-item').removeClass('cookies-item--active');
                }else{
                    $(this).closest('.cookies-item').addClass('cookies-item--active');
                }
            });


            $( ".switcher__item" ).click(function(e) {

                e.stopPropagation()
                if($( this ).hasClass( "switcher__item--toggled" )){
                    if($( this ).hasClass( "analytics" )){
                        cookies.find(cookie => cookie.type == 'analytics').checked = false;
                    }
                    if($( this ).hasClass( "marketing" )){
                        cookies.find(cookie => cookie.type == 'marketing').checked = false;
                    }

                    $(this).removeClass('switcher__item--toggled');
                }else{
                    if($( this ).hasClass( "analytics" )){
                        cookies.find(cookie => cookie.type == 'analytics').checked = true;
                    }
                    if($( this ).hasClass( "marketing" )){
                        cookies.find(cookie => cookie.type == 'marketing').checked = true;
                    }

                    $(this).addClass('switcher__item--toggled');

                }

            });
        }
        function openCookie(){
            $('.cookies').addClass('cookies--active');

        }
        function hideCookie(){
            $('.cookies').removeClass('cookies--active');
            checkCookies();
        }
        function cookiesAllow(){
            console.log(cookies);
            window.localStorage.setItem('cookies', JSON.stringify(cookies))
            $('.cookies').removeClass('cookies--active');
            location.reload();
        }
        function cookiesAllowAll(){
            cookies.forEach(cookie => {
                cookie.checked = true;
            })
            window.localStorage.setItem('cookies', JSON.stringify(cookies))
            $('.cookies').removeClass('cookies--active');
            location.reload();

        }








        $( "#cookie" ).click(function() {
            openCookie();
        });











</script>


<style>

    .cookies{
        content: "";
        position: fixed;
        background: rgba(0, 0, 0, 0.5);
        width: 100%;
        height: 100%;
        z-index: 99999999999;
        visibility: hidden;
        opacity: 0;
        transition: visibility 0s, opacity 0.3s;
        top: 0px;
    }
    .cookies__card{
        overflow: auto;
        background: white;
        width: 100%;
        position: fixed;
        height: 100%;
        max-width: 500px;
        transition: 0.6s;
        left: -500px;
        padding: 15px 20px;
        box-shadow: 0px 0px 50px 0px rgba(0, 0, 0, 0.2);
    }
    .cookies--active{
        visibility: visible;
        opacity: 1;
        transition: visibility 0s, opacity 0.3s;
    }
    .cookies--active .cookies__card{
        transition: 0.6s;
        left: 0px;
    }
    .cookies-top{
        height: 50px;
        padding-bottom: 15px;
        display: flex;
        justify-content: space-between;
        border-bottom: solid 2px #efefef;
    }
    .cookies-top__logo{
        width: 100px;
        height: 100%;
        background-size: contain;
        background-position: left center;
        background-repeat: no-repeat;
        background-image: url(/wp-content/themes/tvin-cones/images/logo.svg)
    }

    .cookies-top__close{
        display: flex;
        align-items: center;
        padding-left: 10px;
        cursor: pointer;
    }
    .cookies-close {
        height: 20px;
        width: 20px;
        position: relative;
        display: block;
    }
    .cookies-close:before{
        content: "";
        top:calc(50% - 1px);
        width: 100%;
        height: 2px;
        background: black;
        position: absolute;
        transform: rotate(45deg);
    }
    .cookies-close:after{
        content: "";
        top:calc(50% - 1px);
        width: 100%;
        height: 2px;
        background: black;
        position: absolute;
        transform: rotate(-45deg);
    }
    .cookies-section{
        padding: 15px 0px;
        border-bottom: solid 2px #efefef;
        padding-bottom: 5px;
    }
    .cookies-section:last-child{
        border-bottom: 0px;
    }
    .cookies-button{
        margin-top: 20px;
        margin-bottom: 20px;
        max-width: 200px;
    }
    .cookies-title{
        font-size: 18px;
        font-weight: 600;
        margin-bottom: 15px;
    }
    .cookies-text{
        font-size: 14px;
        margin-bottom: 10px;
        line-height: 24px;
    }

    .cookies-text--light{
        color: #7e7e7e;
    }
    .cookies-items{
        border: solid 2px #efefef;
    }
    .cookies-item{
        border-bottom: solid 2px #efefef;
        overflow: auto;
    }
    .cookies-item:last-child{
        border-bottom: 0px;
    }
    .cookies-item__tab{
        display: flex;
        justify-content: space-between;

        padding: 15px 15px;
        cursor: pointer;
    }
    .cookies-item-text {
        padding-left: 20px;
        position: relative;
        font-weight: 600;
        font-size: 16px;
        display: flex;
        align-items: center;
        max-width: 320px;
    }
    .cookies-item-text:before{
        transition: 0.3s;
        content:"";
        position: absolute;
        width: 10px;
        height: 2px;
        background: black;
        left: 0px;
        top: calc(50% - 1px);
    }
    .cookies-item-text:after{
        transition: 0.3s;
        content:"";
        position: absolute;
        width: 10px;
        transform: rotate(90deg);
        height: 2px;
        background: black;
        left: 0px;
        top: calc(50% - 1px);
    }
    .cookies-item--active .cookies-item-text:before{
        transition: 0.3s;
        transform: rotate(45deg);
        margin-left: 3px;
        width: 10px;
    }
    .cookies-item--active .cookies-item-text:after{
        transition: 0.3s;
        transform: rotate(135deg);
        margin-left: -3px;
        width: 10px;
    }
    .cookies-item__data{
        padding: 10px 15px;
        padding-top: 0px;
        display: none;
    }
    .cookies-item--active .cookies-item__data{
        display: block;
    }
    .cookies-item-switcher-always-active{
        color: #29A7D6;
        font-weight: bold;
        font-size: 14px;
        display: flex;
        align-items: center;
        min-height: 28px;
    }
    .cookie{
        display: none;
        width: 60px;
        height: 60px;
        background: url(/wp-content/themes/tvin-cones/cookie.svg);
        background-size: contain;
        position: fixed;
        cursor: pointer;
        box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.2);
        border-radius: 100%;
        left: 20px;
        bottom: 20px;
        padding: 12px;
        background-origin: content-box;
        background-position: center;
        background-repeat: no-repeat;
        background-color: #29A7D6;
        z-index: 9999999999;

        transform: rotateY(0deg);
        opacity: 1;
        visibility: visible;
        transition: transform 0.5s, visibility 0s, opacity 0.3s, background-color 0.3s;
    }
    .cookie:hover{
        background-color: #29A7D6;
        transition: 0.3s;
        box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.2);
    }
    .cookies--active ~ .cookie{

        transform: rotateY(90deg);
        opacity: 0;
        visibility: hidden;
        transition: transform 0.5s, visibility 0.5s, opacity 0.3s, background-color 0.3s;
    }
    @media(max-width: 500px){
        .cookie {
            width: 36px;
            height: 36px;
            left: 10px;
            bottom: 10px;
            padding: 8px;
        }
    }
    .cookie--active{
        display: block;
    }



    .switcher__item{
        width: 52px;
        cursor:pointer;
        background: #e9e9e9;
        border-radius: 100px;
        transition: 0.2s;
        height: 28px;
        border:solid 2px #e9e9e9;

    }
    .switcher__item:hover .switcher-button{
        transition: 0.2s;
    }

    .switcher__item__button{
        width: 24px;
        height: 24px;
        border-radius: 100%;

        position: relative;
        line-height: 30px;
        transition: 0.2s;
        text-align: center;
        left: 0px;
        background: #d1d1d1;
    }



    .switcher__item--toggled{
        transition: 0.2s;
        border:solid 2px #29A7D6;
        background: rgba(41, 167, 214, 0.1);


    }
    .switcher__item--toggled .switcher__item__button{
        left: 24px;
        transition: 0.2s;
        background: #29A7D6;
    }

    .switchers{
        height: 100%;
        position: relative;
        width: 50%;
        float: left;
        padding-top: 15px;


    }


    .switcher__text{
        line-height: 40px;
        font-size: 14px;

        width: 70px;
    }
    .switcher__text--left{
        text-align: right;

        padding-right: 8px;
    }
    .switcher__text--right{
        padding-left: 8px;
    }



</style>
<main class="all">


