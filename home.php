<?php /* Template name: Produkty */ ?>
<?php get_header(); ?>



    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary third-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp">Produkty</h1>
    </header>

    <div class="confirm-wrap">
        <div class="confirm">
            <div class="confirm-check">
                <?php get_template_part('svg/ico', 'check') ?>
            </div>
            <span class="confirm-text">
                Zboží bylo přidáno do košíku
            </span>
            <br>
            <button class="btn btn__orange btn__next"><div class="next-text">Pokračovat eee</div></button>
            <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-cart.php' ) ) ?>"><button class="btn btn__orange btn__to-cart">Do košíku</button></a>
        </div>
    </div>

    <div class="sp-content sp-content__margin pr-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="filter-wrap filter-wrap__desktop">
                        <?php
                        $terms = get_terms( 'category', array(
                            'orderby'    => 'count',
                            'hide_empty' => 0
                        ) );



                        echo '<ul>';



                        $i = 0;
                        $len = count($terms);
                        foreach ($terms as $term) {
                            $current_term = is_tax ? get_queried_object() : null;
                            $classactive = "";
                            if($current_term != null && $current_term->term_taxonomy_id == $term->term_taxonomy_id) {
                                $classactive = "active";
                            }

                            if ($i == $len - 1) {
                                echo '<a href="' . get_term_link( $term ) . '"><li class="last ' . $classactive . '">' . $term->name . '</li></a>';
                            }
                            else {
                                echo '<a href="' . get_term_link( $term ) . '"><li class="' . $classactive . '">' . $term->name . '</li></a>';
                            }
                            $i++;
                        }


                        echo '</ul>';
                        ?>

                    </div>
                </div>



                <div class="col-lg-9 product-part "> <!-- Doplnit třídu product__list -->
                    <div class="category-button-wrap">
                        <a href="#kategorie"><button class="btn btn__blue">Kategorie</button></a>
                    </div>
                    <div class="product-listing">
                        <span class="list-display"><?php get_template_part('svg/ico', 'line') ?></span>
                        <span class="grid-display filter-active"><?php get_template_part('svg/ico', 'grid') ?></span>
                        <span class="pdf"><?php get_template_part('svg/ico', 'pdf') ?></span>
                    </div>
                    <h2>Všechny kornouty</h2>




<?php
$paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
$args = array( 'posts_per_page' => 9, 'paged' => $paged);
$loop = new WP_Query( $args );
?>

<?php  while ( $loop->have_posts() ) :$loop->the_post();
    global $post;?>


    <div class="product">
        <div class="product-head">
            <div class="pr-title-wrap">
                <div class="pr-title">
                    <?php the_field('title') ?>
                </div>
            </div>


            <div class="pr-cone">
                <img class="stand" src="<?php the_field('img') ?>"><img class="lie" src="<?php the_field('img') ?>">
            </div>
        </div>
        <div class="data">
            <div class="average">
                <div class="ico-detail"><a href="#">Průměr</a></div>
                <div class="ico-part ico-part__average"><?php get_template_part('svg/ico', 'average') ?></div><div class="text-part"><?php the_field('average') ?> mm</div>
            </div>
            <div class="box">
                <div class="ico-detail">Kusů v balení</div>
                <div class="ico-part ico-part__box"><?php get_template_part('svg/ico', 'package') ?></div><div class="text-part"><?php the_field('package') ?></div>
            </div>
            <div class="meter">
                <div class="ico-detail">Délka</div>
                <div class="ico-part ico-part__meter"><?php get_template_part('svg/ico', 'meter') ?></div><div class="text-part"><?php the_field('meter') ?> mm</div>
            </div>
            <div class="gift">
                <div class="ico-detail">Balení</div>
                <div class="ico-part ico-part__gift"><?php get_template_part('svg/ico', 'gift') ?></div><div class="text-part"><?php the_field('gift') ?></div>
            </div>
        </div>

        <div class="price-wrap">
            <div class="price-text">
                Cena za Ka:
            </div>
            <div class="price">
                <?php the_field('price') ?> Kč
            </div>
        </div>

        <form class="pr-form">
            <input min="1" value="1"><span class="unit">Ka</span>
            <button class="btn btn__orange btn__form btn__add-to-cart">Objednat</button>
        </form>
    </div>



<?php endwhile; ?>












                </div>

                <div class="filter-wrap filter-wrap__tablet" id="kategorie">
                    <?php
                    $terms = get_terms( 'category', array(
                        'orderby'    => 'count',
                        'hide_empty' => 0
                    ) );



                    echo '<ul>';



                    $i = 0;
                    $len = count($terms);
                    foreach ($terms as $term) {
                        $current_term = is_tax ? get_queried_object() : null;
                        $classactive = "";
                        if($current_term != null && $current_term->term_taxonomy_id == $term->term_taxonomy_id) {
                            $classactive = "active";
                        }

                        if ($i == $len - 1) {
                            echo '<a href="' . get_term_link( $term ) . '"><li class="last ' . $classactive . '">' . $term->name . '</li></a>';
                        }
                        else {
                            echo '<a href="' . get_term_link( $term ) . '"><li class="' . $classactive . '">' . $term->name . '</li></a>';
                        }
                        $i++;
                    }


                    echo '</ul>';
                    ?>
                </div>
            </div>
        </div>
    </div>





<?php get_footer(); ?>