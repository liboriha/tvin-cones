
<?php get_header(); ?>


    <?php

    $to = "tvin@tvin-cones.com";
    $subject = "Kontaktní formulář z webu";

    $contact_name = $_POST['contact_name'];
    $contact_second_name = $_POST['contact_second_name'];
    $contact_mail = $_POST['contact_mail'];
    $contact_phone = $_POST['contact_phone'];
    $contact_message = $_POST['contact_message'];
    $contact_submit = $_POST['contact_submit'];


    $message="<!DOCTYPE html>\n";
    $message.="<html  lang=\"cs\">\n";
    $message.="<head>\n";
    $message.="<meta charset=\"UTF-8\">\n";
    $message.="</head>\n";
    $message.="<body>\n";

    $message.="<h1>Kontaktní formulář</h1>";
    $message.="<span><b>Jméno:</b> $contact_name</span><br>";
    $message.="<span><b>Příjmení:</b> $contact_second_name</span><br>";
    $message.="<span><b>E-mail:</b> $contact_mail</span><br>";
    $message.="<span><b>Telefon:</b> $contact_phone</span><br>";
    $message.="<span><b>Zpráva:</b></span><br>";
    $message.="$contact_message";

    $message.="</body>\n";
    $message.="</html>\n";




    if(isset($contact_submit)){

        if(isset($contact_name) && isset($contact_second_name) && isset($contact_mail) && isset($contact_phone) && isset($contact_message)){

            wp_mail($to,$subject,$message);
            if($_GET['lang']=="en"){
                $error_message = "<b class='correct_send'>Message was successfully sent!</b>";
            } else {
                $error_message = "<b class='correct_send'>Zpráva byla úspěšně odeslána!</b>";
            }


        }
        else{
            if($_GET['lang']=="en"){
                $error_message = "<b class='correct_send'>Message has not been sent!</b>";
            } else {
                $error_message = "<b class='correct_send'>Zpráva nebyla odeslána!</b>";
            }

        }
    }





    ?>



    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary fourth-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp">ERROR</h1>
    </header>

<style>
    .error__number{
        font-size: 200px;
        font-weight: bold;
        line-height: normal;
        color: #FFBC6E;
    }
    .error__text{
        color: #255DA0;
        font-weight: bold;
        font-size: 20px;
        position: absolute;
        top: 174px;
        left: 7px;
    }
    .error__reason{
        font-weight: 700;
        color: #e76a3c;
        font-size: 40px;
        line-height: normal;
        position: relative;
        top: -20px;
    }
    .error{
        position: relative;
        width: 420px;
        margin-left: auto;
        margin-right: auto;

    }
    .error-row{
        text-align: center;
    }
</style>

    <div class="sp-content sp-content__margin  sp-content__margin--bottom">
        <div class="container">
            <div class="row error-row">
                <div class="col-md-12">

                    <div class="error">
                        <span class="error__number">404</span>
                        <span class="error__text">Error</span><br>
                        <span class="error__reason"><?php if($_GET['lang']=="en"){echo "page not found";} else {echo "stránka nenalezena";} ?></span>
                    </div>
                    <div class="error-body">
                        <p>
                            Oooops... <br>
                            <?php if($_GET['lang']=="en"){echo "Sorry, it looks like the requested page doesn't exist.";} else {echo "Omlouváme se ale vypadá to, že požadovaná stránka neexistuje.";} ?><br>
                            <?php if($_GET['lang']=="en"){echo "If you will have any further problems, please contact us.";} else {echo "V případě dalších potíží nás prosím kontaktujte.";} ?>

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="map-display" id="map-display" tabindex="-1">
        <div class="h2-marker-wrap">
            <h2 class="h2-map"><?php if($_GET['lang']=="en"){echo "Where to find us?";} else {echo "Kde nás najdete?";} ?></h2>
            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/marker.svg" draggable="false" class="map-marker">
        </div>
        <div class="map-shaddow-top">

        </div>
        <div class="map-shaddow"></div>
    </div>

    <div id="map"></div>


    <script>
        if (window.history.replaceState) {
            window.history.replaceState(null, null, window.location.href);
        }
    </script>

<?php get_footer(); ?>