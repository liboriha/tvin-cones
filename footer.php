
<footer>
    <div class="container">
        <div class="row footer-top-wrap">
            <div class="col-sm-5 footer-top-left">
                <b class="footer-b"><?php if($_GET['lang']=="en"){echo "Address";} else {echo "Adresa";} ?></b><br>
                <?php if($_GET['lang']=="en"){echo "CRN:";} else {echo "IČ:";} ?> <?php the_field('ic', 'options') ?><br>
                <?php if($_GET['lang']=="en"){echo "VAT:";} else {echo "DIČ:";} ?> <?php the_field('dic', 'options') ?><br>
                <?php the_field('firm_name', 'options') ?><br>
                <?php the_field('adress', 'options') ?><br>
                <?php the_field('psc', 'options') ?><br>
                <?php the_field('city', 'options') ?><br>
            </div>
            <div class="col-sm-2 footer-top-logo">
                <?php get_template_part('svg/ico', 'logo') ?>
            </div>
            <div class="col-sm-5 footer-top-right">
                <b class="footer-b"><?php if($_GET['lang']=="en"){echo "Contacts";} else {echo "Kontakty";} ?></b>
                <div class="contact-info">
                    Tel: <a href="tel:<?php the_field('phone', 'options') ?>"><?php the_field('phone', 'options') ?></a><br>
                    GSM: <a href="tel:<?php the_field('gsm_1', 'options') ?>"><?php the_field('gsm_1', 'options') ?></a><br>
                    GSM: <a href="tel:<?php the_field('gsm_2', 'options') ?>"><?php the_field('gsm_2', 'options') ?></a><br>
                    E-mail: <a href="mailto:<?php the_field('mail', 'options') ?>"><?php the_field('mail', 'options') ?></a>
                </div>
                <table>
                    <tr>
                        <td>Tel:</td>
                        <td><a href="tel:<?php the_field('phone', 'options') ?>"><?php the_field('phone', 'options') ?></a></td>
                    </tr>
                    <tr>
                        <td>GSM:</td>
                        <td><a href="tel:<?php the_field('gsm_1', 'options') ?>"><?php the_field('gsm_1', 'options') ?></a></td>
                    </tr>
                    <tr>
                        <td>GSM:</td>
                        <td><a href="tel:<?php the_field('gsm_2', 'options') ?>"><?php the_field('gsm_2', 'options') ?></a></td>
                    </tr>
                    <tr>
                        <td>E-mail:</td>
                        <td><a href="mailto:<?php the_field('mail', 'options') ?>"><?php the_field('mail', 'options') ?></a></td>
                    </tr>
                </table>
                <div>
                    <?php the_field('contact_text', 'options') ?>
                </div>
            </div>
        </div>
        <div class="row footer-bottom-wrap">
            <div class="col-sm-4 col-xs-6 footer-bottom-left">
                © <?php echo date('Y') ?> <?php the_field('firm_name', 'options') ?>
            </div>
            <div class="col-sm-push-4 col-sm-4 col-xs-6 footer-bottom-right">
                <a target="_blank" href="https://peko-studio.cz"><?php if($_GET['lang']=="en"){echo "Made by Peko Studio s.r.o.";} else {echo "Vytvořilo Peko Studio s.r.o.";} ?></a>
            </div>
            <div class="col-sm-pull-4 col-sm-4 col-xs-12 footer-bottom-center">
                <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-certificate.php' ) ) ?>"><?php if($_GET['lang']=="en"){echo "Certificates";} else {echo "Certifikáty";} ?></a>
            </div>

        </div>
    </div>
</footer>







</main>
<?php wp_footer(); ?>
</body>

</html>
