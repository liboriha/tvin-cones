<?php /* Template name: Produkty */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary third-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp"><?php if($_GET['lang']=="en"){echo "Products";} else {echo "Produkty";} ?></h1>
    </header>

    <div class="confirm-wrap confirm-cart">
        <div class="confirm">
            <div class="confirm-check">
                <?php get_template_part('svg/ico', 'check') ?>
            </div>
            <span class="confirm-text">
                <?php if($_GET['lang']=="en"){echo "Product has been successfully added to cart";} else {echo "Zboží bylo přidáno do košíku";} ?>
            </span>
            <br>
            <button class="btn btn__orange btn__next" onclick="window.location.reload()"><div class="next-text"><?php if($_GET['lang']=="en"){echo "Continue";} else {echo "Pokračovat";} ?></div></button>
            <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-cart.php' ) ) ?>"><button class="btn btn__orange btn__to-cart"><?php if($_GET['lang']=="en"){echo "Go to cart";} else {echo "Do košíku";} ?></button></a>
        </div>
    </div>


    <div class="confirm-wrap confirm-photo">
        <div class="confirm">

            <span class="confirm-text">
                <div class="modal-cone">
                    <div class="stand stand-modal"></div>
                  <!--  <div class="lie lie-modal"></div>-->
                </div>
            </span>
            <br>
            <button class="btn btn__orange btn__next" ><div class="next-text"><?php if($_GET['lang']=="en"){echo "Close";} else {echo "Zavřít";} ?></div></button>
        </div>
    </div>


    <div class="confirm-wrap confirm-ingredients">
        <div class="confirm">

            <span class="confirm-text confirm-text--ingredients">
                <div>Složení výrobku</div>
                <p>
                </p>
            </span>
            <br>
            <button class="btn btn__orange btn__next" ><div class="next-text"><?php if($_GET['lang']=="en"){echo "Close";} else {echo "Zavřít";} ?></div></button>
        </div>
    </div>

    <div class="confirm-wrap unconfirm-cart">
        <div class="confirm">
            <div class="confirm-check">
                <?php get_template_part('svg/ico', 'check') ?>
            </div>
            <span class="confirm-text">
                <?php if($_GET['lang']=="en"){echo "The number of items must contain a number";} else {echo "Počet položek musí obsahovat číslo";} ?>

            </span>
            <br>
            <button class="btn btn__orange btn__next"><div class="next-text"><?php if($_GET['lang']=="en"){echo "Continue";} else {echo "Pokračovat";} ?></div></button>
            <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-cart.php' ) ) ?>"><button class="btn btn__orange btn__to-cart"><?php if($_GET['lang']=="en"){echo "Go to cart";} else {echo "Do košíku";} ?></button></a>
        </div>
    </div>

    <div class="sp-content sp-content__margin pr-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">

                    <div class="filter-wrap filter-wrap__desktop">
                        <?php
                        $terms = get_terms( 'type', array(
                            'orderby'    => 'date',
                            'hide_empty' => 0
                        ) );



                        echo '<ul>';



                        $i = 0;
                        $len = count($terms);
                        foreach ($terms as $term) {
                            $current_term = is_tax ? get_queried_object() : null;
                            $classactive = "";
                            if($current_term != null && $current_term->term_taxonomy_id == $term->term_taxonomy_id) {
                                $classactive = "active";
                            }

                            if ($i == $len - 1) {
                                echo '<a href="' . get_term_link( $term ) . '"><li class="last ' . $classactive . '">' . $term->name . '</li></a>';
                            }
                            else {
                                echo '<a href="' . get_term_link( $term ) . '"><li class="' . $classactive . '">' . $term->name . '</li></a>';
                            }
                            $i++;
                        }


                        echo '</ul>';
                        ?>
                    </div>
                </div>
                <div class="col-lg-9 product-part "> <!-- Doplnit třídu product__list -->
                   <div class="category-button-wrap">
                       <a href="#kategorie"><button class="btn btn__blue"><?php if($_GET['lang']=="en"){echo "Categories";} else {echo "Kategorie";} ?></button></a>
                   </div>
                    <div class="product-listing">
                        <span class="list-display"><?php get_template_part('svg/ico', 'line') ?></span>
                        <span class="grid-display filter-active"><?php get_template_part('svg/ico', 'grid') ?></span>
                        <a target="_blank" href="<?php the_field('pdf_cenik','options') ?>"><span class="pdf"><?php get_template_part('svg/ico', 'pdf') ?></span></a>
                    </div>
                <h2><?php if($_GET['lang']=="en"){echo "All cones";} else {echo "Všechny kornouty";} ?></h2>





    <?php
    $paged = ( get_query_var( 'paged' )) ? get_query_var( 'paged' ) : 1;
    $args = array( 'post_type' => 'kornouty', 'posts_per_page' => 9, 'paged' => $paged, 'orderby' => 'date',  'order' => 'ASC',);
    $loop = new WP_Query( $args );
    ?>

    <?php  while ( $loop->have_posts() ) :$loop->the_post();
        global $post;?>

        <div class="product">
                        <div class="product-head">
                            <div class="pr-title-wrap">
                                <div class="pr-title">
                                    <?php the_field('title') ?>
                                </div>
                            </div>

                            <?php if(get_field('catalog_number')){ ?>
                                <div class="pr-catalog-num">Kat.č.: <?php the_field('catalog_number') ?></div>
                            <?php } ?>

                            <?php if(get_field('ingredients')){ ?>
                                <div class="pr-ingredients" ingredients="<?php the_field('ingredients') ?>">Složení</div>
                            <?php } ?>

                            <div class="pr-cone" image="<?php the_field('img') ?>">
                                <img class="stand" src="<?php the_field('img') ?>"><img class="lie" src="<?php the_field('img') ?>">
                            </div>


                        </div>
                        <div class="data">
                            <?php if(get_field('average')){ ?>
                                <div class="average">
                                    <div class="ico-detail"><a href="#"><?php if($_GET['lang']=="en"){echo "Average";} else {echo "Průměr";} ?></a></div>
                                    <div class="ico-part ico-part__average"><?php get_template_part('svg/ico', 'average') ?></div><div class="text-part"><?php the_field('average') ?> mm</div>
                                </div>
                            <?php } ?>

                            <?php if(get_field('package')){ ?>
                                <div class="box">
                                    <div class="ico-detail"><?php if($_GET['lang']=="en"){echo "Pieces in box";} else {echo "Kusů v balení";} ?></div>
                                    <div class="ico-part ico-part__box"><?php get_template_part('svg/ico', 'package') ?></div><div class="text-part"><?php the_field('package') ?></div>
                                </div>
                            <?php } ?>

                            <?php if(get_field('meter')){ ?>
                                <div class="meter">
                                    <div class="ico-detail"><?php if($_GET['lang']=="en"){echo "Length";} else {echo "Délka";} ?></div>
                                    <div class="ico-part ico-part__meter"><?php get_template_part('svg/ico', 'meter') ?></div><div class="text-part"><?php the_field('meter') ?> mm</div>
                                </div>
                            <?php } ?>

                            <?php if(get_field('gift')){ ?>
                                <div class="gift">
                                    <div class="ico-detail"><?php if($_GET['lang']=="en"){echo "Package";} else {echo "Balení";} ?></div>
                                    <div class="ico-part ico-part__gift"><?php get_template_part('svg/ico', 'gift') ?></div><div class="text-part"><?php the_field('gift') ?></div>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="price-wrap">
                            <div class="price-text">
                                <?php if($_GET['lang']=="en"){echo "Price per Ka:";} else {echo "Cena za Ka:";} ?>
                            </div>
                            <div class="price">

                                <?php the_field('price') ?>
                                <?php if(get_field('price') != 'Na dotaz'){


                                    $price_dph = get_field('price_dph');
                                    ?>
                                    Kč <span class="without_dph"><?php $price = get_field('price'); $result = $price * $price_dph ?>Cena s DPH: <?php echo $price + round_up($result, 2); ?> Kč </span>

                                <?php }  ?>
                            </div>
                        </div>


                        <form class="pr-form js-pr-form">

                            <input type="number" class="js-product-qty" min="1" value="1"><span class="unit">Ka</span>
                            <button data-product-id="<?php echo get_the_ID(); ?>" class="btn btn__orange btn__form btn__add-to-cart"><?php if($_GET['lang']=="en"){echo "Order";} else {echo "Objednat";} ?></button>
                        </form>
                    </div>

<?php endwhile; ?>


                    <?php if( $paged >= 1 ): ?>
                        <div class="navi-buttons col-xs-12">

                            <?php
                            if($_GET['lang']=="en"){
                                $predchozi = "Previous cones";
                                $nasledujici = "Next cones";
                            } else {
                                $predchozi = "Přechozí kornouty";
                                $nasledujici = "Další kornouty";
                            }
                            ?>
                            <?php previous_posts_link("<button class=\"btn btn__blue\">" . $predchozi . "</button>"); ?>

                            <?php next_posts_link("<button class=\"btn btn__blue\">" . $nasledujici . "</button>", $loop->max_num_pages); ?>

                        </div>
                    <?php endif; ?>

                </div>











                <div class="filter-wrap filter-wrap__tablet" id="kategorie">
                    <?php
                    $terms = get_terms( 'type', array(
                        'orderby'    => 'date',
                        'hide_empty' => 0
                    ) );



                    echo '<ul>';



                    $i = 0;
                    $len = count($terms);
                    foreach ($terms as $term) {
                        $current_term = is_tax ? get_queried_object() : null;
                        $classactive = "";
                        if($current_term != null && $current_term->term_taxonomy_id == $term->term_taxonomy_id) {
                            $classactive = "active";
                        }

                        if ($i == $len - 1) {
                            echo '<a href="' . get_term_link( $term ) . '"><li class="last ' . $classactive . '">' . $term->name . '</li></a>';
                        }
                        else {
                            echo '<a href="' . get_term_link( $term ) . '"><li class="' . $classactive . '">' . $term->name . '</li></a>';
                        }
                        $i++;
                    }


                    echo '</ul>';
                    ?>
                </div>
            </div>
        </div>
    </div>




<?php endwhile; ?>
<?php get_footer(); ?>