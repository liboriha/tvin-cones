<?php /* Template name: Kontakt */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php

    $to = "tvin@tvin-cones.com";
    $subject = "Kontaktní formulář z webu";

    $contact_name = $_POST['contact_name'];
    $contact_second_name = $_POST['contact_second_name'];
    $contact_mail = $_POST['contact_mail'];
    $contact_phone = $_POST['contact_phone'];
    $contact_message = $_POST['contact_message'];
    $contact_submit = $_POST['contact_submit'];


    $message="<!DOCTYPE html>\n";
    $message.="<html  lang=\"cs\">\n";
    $message.="<head>\n";
    $message.="<meta charset=\"UTF-8\">\n";
    $message.="</head>\n";
    $message.="<body>\n";

    $message.="<h1>Kontaktní formulář</h1>";
    $message.="<span><b>Jméno:</b> $contact_name</span><br>";
    $message.="<span><b>Příjmení:</b> $contact_second_name</span><br>";
    $message.="<span><b>E-mail:</b> $contact_mail</span><br>";
    $message.="<span><b>Telefon:</b> $contact_phone</span><br>";
    $message.="<span><b>Zpráva:</b></span><br>";
    $message.="$contact_message";

    $message.="</body>\n";
    $message.="</html>\n";




    if(isset($contact_submit)){

        if(isset($contact_name) && isset($contact_second_name) && isset($contact_mail) && isset($contact_phone) && isset($contact_message)){

            wp_mail($to,$subject,$message);
            if($_GET['lang']=="en"){
                $error_message = "<b class='correct_send'>Message was successfully sent!</b>";
            } else {
                $error_message = "<b class='correct_send'>Zpráva byla úspěšně odeslána!</b>";
            }


        }
        else{
            if($_GET['lang']=="en"){
                $error_message = "<b class='correct_send'>Message has not been sent!</b>";
            } else {
                $error_message = "<b class='correct_send'>Zpráva nebyla odeslána!</b>";
            }

        }
    }





    ?>



    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary fourth-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp"><?php if($_GET['lang']=="en"){echo "Contact";} else {echo "Kontakt";} ?></h1>
    </header>



    <div class="sp-content sp-content__margin  sp-content__margin--bottom">
        <div class="container">
            <div class="row contact-row">
                <div class="col-md-6 col-sm-7 col-left-contact">
                    <div class="contact-form-card">
                        <h2><?php if($_GET['lang']=="en"){echo "Write us";} else {echo "Napište nám";} ?></h2>
                        <?php echo $error_message; ?>
                        <div class="row">
                            <?php if($_GET['lang']=="en"){
                                echo do_shortcode('[contact-form-7 id="948" title="Kontaktní formulář 1_en"]');
                            } else {
                                echo do_shortcode('[contact-form-7 id="947" title="Kontaktní formulář 1"]');
                            } ?>

                            <!--<form id="contact_form" method="post" action="<?php the_permalink( getPageIDByTemplate( 'page-template/page-contact.php' ) ) ?>">
                                <div class="col-md-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Name";} else {echo "Jméno";} ?> *</span><br>
                                    <input name="contact_name" required type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your name";} else {echo "Vaše jméno";} ?>">
                                </div>
                                <div class="col-md-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Second name";} else {echo "Příjmení";} ?> *</span><br>
                                    <input name="contact_second_name" required type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Sour second name";} else {echo "Vaše příjmení";} ?>">
                                </div>
                                <div class="col-md-6">
                                    <span class="form-span">E-mail *</span><br>
                                    <input name="contact_mail" required type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your e-mail";} else {echo "Váš e-mail";} ?>">
                                </div>
                                <div class="col-md-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Phone";} else {echo "Telefon";} ?> *</span><br>
                                    <input name="contact_phone" required type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your phone";} else {echo "Váš telefon";} ?>">
                                </div>
                                <div class="col-md-12">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Message";} else {echo "Zpráva";} ?> *</span><br>
                                    <textarea name="contact_message" required placeholder="<?php if($_GET['lang']=="en"){echo "Your text";} else {echo "Váš text";} ?>"></textarea>
                                </div>
                                <div class="col-md-12 contact-btn-wrap">
                                    <button name="contact_submit" type="submit" id="contact_submit" class="btn btn__orange btn__contact"><?php if($_GET['lang']=="en"){echo "Send";} else {echo "Odeslat";} ?></button>
                                </div>

                            </form>-->
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-5 col-right-contact">
                    <h2><?php if($_GET['lang']=="en"){echo "Where to find us?";} else {echo "Kde nás najdete?";} ?></h2>
                    <div class="first-contact-info">
                        <h3><b><?php if($_GET['lang']=="en"){echo "Place";} else {echo "Sídlo";} ?></b></h3>
                        <?php the_field('adress_s', 'options') ?><br>
                        <?php the_field('psc_s', 'options') ?><br>
                        <?php the_field('city_s', 'options') ?><br>
                        <br>
                        <h3><b><?php if($_GET['lang']=="en"){echo "Establishment";} else {echo "Provozovna";} ?></b></h3>
                        <?php the_field('adress_p', 'options') ?><br>
                        <?php the_field('psc_p', 'options') ?><br>
                        <?php the_field('city_p', 'options') ?><br>
                    </div>
                    <div class="second-contact-info">
                        <table>
                            <tr>
                                <td>Tel:</td>
                                <td><a href="tel:<?php the_field('phone', 'options') ?>"><?php the_field('phone', 'options') ?></a></td>
                            </tr>
                            <tr>
                                <td>GSM:</td>
                                <td><a href="tel:<?php the_field('gsm_1', 'options') ?>"><?php the_field('gsm_1', 'options') ?></a></td>
                            </tr>
                            <tr>
                                <td>GSM:</td>
                                <td><a href="tel:<?php the_field('gsm_2', 'options') ?>"><?php the_field('gsm_2', 'options') ?></a></td>
                            </tr>
                            <tr>
                                <td>E-mail:</td>
                                <td><a href="mailto:<?php the_field('mail', 'options') ?>"><?php the_field('mail', 'options') ?></td>
                            </tr>
                        </table>
                    </div>
                    <h2><?php if($_GET['lang']=="en"){echo "Billing information";} else {echo "Fakturační údaje";} ?></h2>
                    <div class="third-contact-info">
                        <?php if($_GET['lang']=="en"){echo "CRN:";} else {echo "IČ:";} ?> <?php the_field('ic', 'options') ?><br>
                        <?php if($_GET['lang']=="en"){echo "VAT:";} else {echo "DIČ:";} ?> <?php the_field('dic', 'options') ?><br>
                        <?php the_field('firm_name', 'options') ?><br>
                        <?php the_field('adress', 'options') ?><br>
                        <?php the_field('psc', 'options') ?><br>
                        <?php the_field('city', 'options') ?><br><br>
                        <?php the_field('contact_text', 'options') ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h2><?php if($_GET['lang']=="en"){echo "Our team";} else {echo "Náš tým";} ?></h2>


                    <div class="first-contact">
                    <!--    <div class="contact-img-wrap-main">
                            <div class="contact-img-wrap">
                                <img class="contact-img" src="<?php /*the_field('img') */?>">
                            </div>
                        </div>-->

                        <div class="contact-info-wrap">
                            <span class="contact-name">
                                <?php the_field('name') ?><br>
                                <?php the_field('second_name') ?>
                            </span>
                            <span class="contact-email"><?php the_field('contact_email') ?></span>
                            <p class="contact-function"><?php the_field('function') ?></p>
                        </div>
                    </div>



                    <div class="second-contact">
                       <!-- <div class="contact-img-wrap-main">
                            <div class="contact-img-wrap">
                                <img class="contact-img" src="<?php /*the_field('img_2') */?>">
                            </div>
                        </div>-->
                        <div class="contact-info-wrap">
                            <span class="contact-name">
                                <?php the_field('name_2') ?><br>
                                <?php the_field('second_name_2') ?>
                            </span>
                            <span class="contact-email"><?php the_field('contact_email_2') ?></span>
                            <p class="contact-function"><?php the_field('function_2') ?></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="people-img"></div>

        </div>
    </div>

    <div class="map-display" id="map-display" tabindex="-1">
        <div class="h2-marker-wrap">
            <h2 class="h2-map"><?php if($_GET['lang']=="en"){echo "Where to find us?";} else {echo "Kde nás najdete?";} ?></h2>
            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/marker.svg" draggable="false" class="map-marker">
        </div>
        <div class="map-shaddow-top">

        </div>
        <div class="map-shaddow"></div>
    </div>

    <div id="map"></div>


<script>
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
</script>
<?php endwhile; ?>
<?php get_footer(); ?>