<?php /* Template name: Akce */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary fourth-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp"><?php if($_GET['lang']=="en"){echo "About us";} else {echo "Akce";} ?></h1>
    </header>


    <!-- akce content -->
    <?php if(get_field("akce_1")){?>
    <div class="sp-content " >
    <?php }; ?>
        <div class="container">
         <?php if(get_field("akce_1")){ ?>
         <div class="row new-card" style="    margin-bottom: 50px;">
        <div class="col-md-6 card-img-section">
            <img src="<?php the_field("akce_1_obrazek");?>" alt="" class="img-img">
       <!--
            <img src="<?php /*echo get_stylesheet_directory_uri(); */?>/images/logo.png" alt="tvin-cones-logo" class="img-img-logo">-->
        </div>
        <div class="col-md-6 card-action-content">
            <h1 class="card-action-content__title"><?php the_field("akce_1") ?></h1>
            <p class="card-action-content__paragraph">
                <?php the_field("akce_1_popis") ?>
            </p>
            <a href="<?php the_field("akce_1_odkaz_url"); ?>">
                <button class="card-action-content__button btn btn__orange">
                    <?php the_field("akce_1_odkaz_text"); ?>
                </button>
            </a>
        </div>
    </div>
   <?php } ?>

   <!-- akce 2 -->
   <?php if(get_field("akce_2")){ ?>
         <div class="row new-card"style="    margin-bottom: 50px;">
        <div class="col-md-6 card-img-section">
            <img src="<?php the_field("akce_2_obrazek");?>" alt="" class="img-img">
          <!--  <img src="<?php /*echo get_stylesheet_directory_uri(); */?>/images/logo.png" alt="tvin-cones-logo" class="img-img-logo">-->
        </div>
        <div class="col-md-6 card-action-content">
            <h1 class="card-action-content__title"><?php the_field("akce_2") ?></h1>
            <p class="card-action-content__paragraph">
                <?php the_field("akce_2_popis") ?>
            </p>
            <a href="<?php the_field("akce_2_odkaz_url"); ?>">
                <button class="card-action-content__button btn btn__orange">
                    <?php the_field("akce_2_odkaz_text"); ?>
                </button>
            </a>
        </div>
    </div>
   <?php } ?>

      <!-- akce 3 -->
      <?php if(get_field("akce_3")){ ?>
         <div class="row new-card"style="    margin-bottom: 50px;">
        <div class="col-md-6 card-img-section">
            <img src="<?php the_field("akce_3_obrazek");?>" alt="" class="img-img">
       <!--     <img src="<?php /*echo get_stylesheet_directory_uri(); */?>/images/logo.png" alt="tvin-cones-logo" class="img-img-logo">-->
        </div>
        <div class="col-md-6 card-action-content">
            <h1 class="card-action-content__title"><?php the_field("akce_3") ?></h1>
            <p class="card-action-content__paragraph">
                <?php the_field("akce_3_popis") ?>
            </p>
            <a href="<?php the_field("akce_3_odkaz_url"); ?>">
                <button class="card-action-content__button btn btn__orange">
                    <?php the_field("akce_3_odkaz_text"); ?>
                </button>
            </a>
        </div>
    </div>
   <?php } ?>
        </div>
    </div>


    <div class="banner-sp">
        <?php get_template_part('parts/category', 'banner') ?>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>