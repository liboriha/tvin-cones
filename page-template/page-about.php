<?php /* Template name: O Nás */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary second-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp"><?php if($_GET['lang']=="en"){echo "About us";} else {echo "O nás";} ?></h1>
    </header>



    <div class="sp-content sp-content__margin">
        <div class="container">
            <div class="row">
                <div class="col-md-6 about-col">
                    <p class="p-bigger"><?php the_field('p_bigger_left') ?></p>
                    <p><?php the_field('p_left') ?></p>

                </div>
                <div class="col-md-6 about-col">
                    <div class="about-img-wrap about-img-wrap__right">
                        <img class="about-img" src="<?php the_field('img_first') ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-6 about-col">
                    <p><?php the_field('p_right') ?></p>

                </div>

                <div class="col-md-6 col-md-pull-6 about-col">
                    <div class="about-img-wrap about-img-wrap__left">
                        <img class="about-img" src="<?php the_field('img_second') ?>">
                    </div>
                </div>
            </div>
                <!--  výjimečný stav -->
                <hr style="margin:0 auto; width:44%;"/>
            <div class="row" style="margin-top:36px;margin-bottom:16px;">

                <div class="col-md-6  pandemic-sup-paragraph">
                    <p>
                    V průběhu výjimečného stavu v naší republice jsme podpořili naše zdravotníky v nemocnicích a účastníme se projektu „pomoc nemocnicím“. Do nemocnic dodáváme trubičky ke kávě pro lékaře a sestřičky. Děkujeme jim tímto za jejich péči, kterou věnují všem pacientům v době koronavirové pandemie.
                    </p>
                </div>
                <div class="col-md-6 pandemic-sup-logo" style="text-align: right">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pomoc_nemocnym_logo.png" alt="tvin-cones-logo" class="img-img-logo">

                </div>
            </div>
            <hr style="margin:0 auto; width:44%;"/>
            <!-- / výjimečný stav -->

        </div>
    </div>

    <div class="sp-content sp-content__orange  sp-content__margin--bottom">
        <div class="container">
            <div class="row">

                <div class="col-md-4">
                    <div class="icon-about-wrap">
                        <div class="icon icon-about">
                            <?php get_template_part('svg/ico', 'calendar') ?>
                        </div>
                    </div>

                    <div class="icon-about-info">
                        <div class="number-about"><?php the_field('years') ?></div>
                        <p><?php the_field('text_years') ?></p>
                    </div>
                </div>



                <div class="col-md-4">
                    <div class="icon-about-wrap">
                        <div class="icon icon-about">
                            <?php get_template_part('svg/ico', 'ice-cream') ?>
                        </div>
                    </div>

                    <div class="icon-about-info">
                        <div class="number-about"><?php the_field('made') ?></div>
                        <p><?php the_field('text_made') ?></p>
                    </div>
                </div>



                <div class="col-md-4">
                    <div class="icon-about-wrap">
                        <div class="icon icon-about">
                            <?php get_template_part('svg/ico', 'thumb') ?>
                        </div>
                    </div>

                    <div class="icon-about-info">
                        <div class="number-about"><?php the_field('customers') ?></div>
                        <p><?php the_field('text_customers') ?></p>
                    </div>
                </div>



                <div class="col-md-6 about-margin">
                    <p><?php the_field('p_left_certificate') ?></p>
                </div>
                <div class="col-md-6 about-margin">
                    <div class="certificate-about-wrap">
                        <div class="certificate-about">
                            <img class="certificate-about-img certificate-about-img__left" src="<?php echo get_stylesheet_directory_uri() ?>/images/certificate/registration.jpg">
                            <img class="certificate-about-img certificate-about-img__right" src="<?php echo get_stylesheet_directory_uri() ?>/images/certificate/registrationcz.jpg">
                        </div>
                        <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-certificate.php' ) ) ?>"><button class="btn btn__orange btn__certificate-about"><?php if($_GET['lang']=="en"){echo "More certificates";} else {echo "Více certifikátů";} ?></button></a>
                    </div>


                </div>




                <div class="col-md-12 about-margin">
                    <p><?php the_field('p_bottom_certificate') ?></p>
                </div>

            </div>
        </div>
    </div>




<div class="banner-sp">
    <?php get_template_part('parts/category', 'banner') ?>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>