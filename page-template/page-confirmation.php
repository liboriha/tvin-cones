<?php /* Template name: Potvrzení */ ?>
<?php if (isset($_POST['confirm_order'])){ ?>
<?php get_header(); ?>



    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary second-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp"><?php if($_GET['lang']=="en"){echo "Order completion";} else {echo "Dokončení objednávky";} ?></h1>
    </header>



    <div class="confirmation-content sp-content sp-content__margin sp-content__margin--bottom">
        <div class="container">


            <?php
            $to = "tvin@tvin-cones.com";
            //$to = "mri3east@seznam.cz";
            $subject = "Poptávka z webu";

            $name = $_POST['o_name'];
            $second_name = $_POST['o_second_name'];
            $company = $_POST['o_company'];
            $ic = $_POST['o_ic'];
            $email = $_POST['o_email'];
            $phone = $_POST['o_phone'];
            $street = $_POST['o_street'];
            $city = $_POST['o_city'];
            $psc = $_POST['o_psc'];

            $del_name = $_POST['o_del_name'];
            $del_second_name = $_POST['o_del_second_name'];
            $del_company = $_POST['o_del_company'];
            $del_ic = $_POST['o_del_ic'];
            $del_email = $_POST['o_del_email'];
            $del_phone = $_POST['o_del_phone'];
            $del_street = $_POST['o_del_street'];
            $del_city = $_POST['o_del_city'];
            $del_psc = $_POST['o_del_psc'];

            $comment = $_POST['o_comment'];

            $payment = $_POST['o_payment'];
            if  ($payment="invoice"){
                $payment_type = "Hotově při převzetí";
            }
            elseif ($payment='cash'){
                $payment_type = "Platba v hotovosti při převzetí";
            }
            else{
                $payment_type = "Platba nebyla definována";
            }

            $delivery = $_POST['o_delivery_info'];


            if( !session_id() ) {
                session_start();
            }

            $total_amount = 0;


            foreach ($_SESSION['products'] as $product) {
                $total_amount += $product['qty'] * get_field('price', $product['id']);
            };







            $message="<!DOCTYPE html>\n";
            $message.="<html  lang=\"cs\">\n";
            $message.="<head>\n";
            $message.="<meta charset=\"UTF-8\">\n";
            $message.="</head>\n";
            $message.="<body>\n";
            $message.="<h1>Objednávka</h1>";


            $message.="<h2>Fakturační údaje</h2>";

            $message.="<span><b>Jméno:</b> $name</span><br>";
            $message.="<span><b>Příjmení:</b> $second_name</span><br>";
            $message.="<span><b>Společnost:</b> $company</span><br>";
            $message.="<span><b>IČ:</b> $ic</span><br>";
            $message.="<span><b>E-mail:</b> $email</span><br>";
            $message.="<span><b>Telefon:</b> $phone</span><br>";
            $message.="<span><b>Ulice a č.p.:</b> $street</span><br>";
            $message.="<span><b>Město:</b> $city</span><br>";
            $message.="<span><b>PSČ:</b> $psc</span>";


            $message.="<h2>Doručovací údaje</h2>";

            if($delivery){
                $message.="<span><b>Jméno:</b> $del_name</span><br>";
                $message.="<span><b>Příjmení:</b> $del_second_name</span><br>";
                $message.="<span><b>Společnost:</b> $del_company</span><br>";
                $message.="<span><b>IČ:</b> $del_ic</span><br>";
                $message.="<span><b>E-mail:</b> $del_email</span><br>";
                $message.="<span><b>Telefon:</b> $del_phone</span><br>";
                $message.="<span><b>Ulice a č.p.:</b> $del_street</span><br>";
                $message.="<span><b>Město:</b> $del_city</span><br>";
                $message.="<span><b>PSČ:</b> $del_psc</span>";
            }
            else{
                $message.="<span><b>Jméno:</b> $name</span><br>";
                $message.="<span><b>Příjmení:</b> $second_name</span><br>";
                $message.="<span><b>Společnost:</b> $company</span><br>";
                $message.="<span><b>IČ:</b> $ic</span><br>";
                $message.="<span><b>E-mail:</b> $email</span><br>";
                $message.="<span><b>Telefon:</b> $phone</span><br>";
                $message.="<span><b>Ulice a č.p.:</b> $street</span><br>";
                $message.="<span><b>Město:</b> $city</span><br>";
                $message.="<span><b>PSČ:</b> $psc</span>";
            }

            $message.="<br><br>";

            $message.="<span><b>Poznámky:</b></span>";
            $message.="$comment";

            $message.="<br>";

            $message.="<h2>Zboží</h2>";


            if( !session_id() ) {
                session_start();
            }


            if($_SESSION['products']):
                $message.="<span>Ceny produktů jsou uvedeny bez DPH</span><br>";
                $price_dph = 0;
                foreach ($_SESSION['products'] as $product) {
                    $product_qty = $product['qty'];
                    $product_id = $product['id'];
                    $product_price = ($product_qty * get_field('price', "$product_id"));
                    $product_title = get_field('title',"$product_id");

                    $price_dph += $product_qty * get_field('price', "$product_id") * get_field('price_dph', "$product_id");


                    $message.="<span><b>Název:</b>&nbsp;$product_title | </span>";
                    $message.="<span><b>Počet:</b>&nbsp;$product_qty Ka | </span>";
                    $message.="<span><b>Cena:</b>&nbsp;$product_price Kč</span><br>";


                };
            endif;


            $total_amount_ceil = round_up($total_amount, 2);

            $total_amount_ceil_dph = $total_amount_ceil * 0.21;
            $total_amount_ceil_with_dph = $total_amount_ceil + round_up($price_dph, 2);
            $total_dph = round_up($price_dph, 2);

            $message.="<br><span><b>Způsob platby:</b> $payment_type</span><br><br>";

            $message.="<span><b>Celková cena bez DPH:</b> $total_amount_ceil Kč</span><br>";
            $message.="<span>DPH: $total_dph Kč</span><br><br>";
            $message.="<span><b>Celková cena s DPH:</b> $total_amount_ceil_with_dph Kč</span>";

            $message.="</body>\n";
            $message.="</html>\n";


            ?>






            <?php if($total_amount_ceil_with_dph > 0){ ?>
                <?php if(wp_mail( $to, $subject, $message)){
                    session_destroy();
                    ?>


                    <h2 class="confirmation-h2">Objednávka byla úspěšně odeslána</h2>
                    <p>Děkujeme za vaši poptávku.</p>
                    <p>V co nejbližší době poptávku zpracujeme a budeme vás kontaktovat!</p>
                    <p>V případě dotazů nás neváhejte kontaktovat na e-mailové adrese: <a href="mailto:tvin@tvin-cones.com">tvin@tvin-cones.com</a></p>



                <?php } else { ?>

                    <h2 class="confirmation-h2">Objednávku se nepodařilo odeslat</h2>
                    <p>Omlouváme se, ale někde se stala chyba.</p>
                    <p>Zkuste to prosím znovu</p>
                    <p>V případě dalších problémů nás neváhejte kontaktovat na e-mailové adrese: <a href="mailto:tvin@tvin-cones.com">tvin@tvin-cones.com</a></p>

                <?php } ?>
            <?php }else{ ?>

                <h2 class="confirmation-h2">Objednávku se nepodařilo odeslat</h2>
                <p>Omlouváme se, ale Váš košík je prázdný.</p>
                <p>Přidejte jakýkoliv produkt do košíku a zkuste to prosím znovu.</p>
                <p>V případě dalších problémů nás neváhejte kontaktovat na e-mailové adrese: <a href="mailto:tvin@tvin-cones.com">tvin@tvin-cones.com</a></p>

            <?php } ?>
        </div>
    </div>






<?php } else { ?>

    <script>
        window.location.href = "/kosik";
    </script>

<?php } ?>
<?php get_footer(); ?>