<?php /* Template name: Certifikáty */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>


    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary second-menu-wrap">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp"><?php if($_GET['lang']=="en"){echo "Certificates";} else {echo "Certifikáty";} ?></h1>
    </header>



    <div class="sp-content sp-content__margin sp-content__margin--bottom">
        <div class="container">
            <div class="row">

                <?php if( have_rows('certificate') ){
                    $i=0;
                    ?>
                    <?php while ( have_rows('certificate') ) : the_row(); $i++ ?>


                        <div class="col-md-4 certificate-col">
                            <div class="certificate-wrap">
                                <h2><?php the_sub_field('title') ?></h2>
                                <p><?php the_sub_field('short_text') ?></p>
                                <img class="certificate-img certificate-img--btn" onclick="displayModal(<?php echo $i; ?>)" src="<?php the_sub_field('img') ?>">
                            </div>
                        </div>

                        <div class="display-modal display-modal-<?php echo $i; ?>">
                            <div class="certificate-modal modal-<?php echo $i; ?>">
                                <div class="certificate-modal__wrap">
                                    <img class="certificate-img certificate-img--modal" src="<?php the_sub_field('img') ?>">
                                </div>
                            </div>
                        </div>


                    <?php endwhile; ?>
                <?php } ?>

            </div>
        </div>
    </div>





    <div class="banner-sp">
        <?php get_template_part('parts/category', 'banner') ?>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>