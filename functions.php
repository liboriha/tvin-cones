<?php


// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 830;

add_action( 'after_setup_theme', 'tvin_setup' );
if ( ! function_exists( 'tvin_setup' ) ):
    function tvin_setup() {

        // This theme styles the visual editor with editor-style.css to match the theme style.
        add_editor_style( 'dist/editor-style.css' );

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'primary'   => __( 'Menu', 'tvin' )
        ) );

        add_theme_support( 'html5', array( 'gallery', 'caption', 'search-form' ) );
        add_theme_support( 'title-tag' );
        add_editor_style( 'dist/editor-style.css' );
        add_theme_support( 'post-thumbnails', array( 'post' ) );

        add_image_size('small_gallery', 164, 133, true);
        add_image_size('ico', 100, 100, true);
        add_image_size('banner_responsive', 991, 300, true);
        add_image_size('contact_img', 412, 266, true);
        add_image_size('product_img', 500, 500, true);
        add_image_size('square_bg', 768, 768, true);
    }
endif;




add_action( 'init', 'cpt_products' );
add_action( 'init', 'tax_custom_products' );


function cpt_products() {
    $labels = array(
        'name'                => _x( 'Kornouty', 'Post Type General Name', 'tvin-cones' ),
        'singular_name'       => _x( 'Kornouty', 'Post Type Singular Name', 'tvin-cones' ),
        'menu_name'           => __( 'Kornouty', 'tvin-cones' ),
        'name_admin_bar'      => __( 'Kornouty', 'tvin-cones' ),
        'parent_item_colon'   => __( 'Nadřazené kornouty:', 'tvin-cones' ),
        'all_items'           => __( 'Všechny kornouty', 'tvin-cones' ),
        'add_new_item'        => __( 'Přidat nový kornout', 'tvin-cones' ),
        'add_new'             => __( 'Přidat nový kornout', 'tvin-cones' ),
        'new_item'            => __( 'Nový kornout', 'tvin-cones' ),
        'edit_item'           => __( 'Upravit kornout', 'tvin-cones' ),
        'update_item'         => __( 'Nahrát kornout', 'tvin-cones' ),
        'view_item'           => __( 'Zobrazit kornout', 'tvin-cones' ),
        'search_items'        => __( 'Hledat kornouty', 'tvin-cones' ),
        'not_found'           => __( 'Nenalezeno', 'tvin-cones' ),
        'not_found_in_trash'  => __( 'Nenalezeno v koši', 'tvin-cones' ),
    );

    $args = array(
        'label'               => __( 'Kornouty', 'tvin-cones' ),
        'description'         => __( 'Kornouty', 'tvin-cones' ),
        'labels'              => $labels,
        'supports'            => array(),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'           => array('slug' => 'kornout')
    );
    register_post_type( 'kornouty', $args );
}



function tax_custom_products() {

    $labels = array(
        'name'              => _x( 'Druh kornoutu', 'Druh kornoutu' ),
        'singular_name'     => _x( 'Druh kornoutu', 'Druh kornoutu' ),
        'search_items'      => __( 'Vyhledat druh kornoutu' ),
        'all_items'         => __( 'Všechny druhy kornoutů' ),
        'parent_item'       => __( 'Nadřazený druh kornoutu' ),
        'parent_item_colon' => __( 'Nadřazené druhy kornoutů:' ),
        'edit_item'         => __( 'Editovat druh kornoutu' ),
        'update_item'       => __( 'Aktualizovat druh kornoutu' ),
        'add_new_item'      => __( 'Přidat nový druh kornoutu' ),
        'new_item_name'     => __( 'Nová druh kornoutu' ),
        'menu_name'         => __( 'Druh kornoutu' )
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'kategorie-kornoutu' )
    );

    register_taxonomy( 'type', ['kornouty'], $args );
}

















if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Šablona',
        'menu_title'	=> 'Šablona',
        'menu_slug' 	=> 'info',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Kontaktní informace',
        'menu_title'	=> 'Kontaktní informace',
        'parent_slug'	=> 'info'
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Banner',
        'menu_title'	=> 'Banner',
        'parent_slug'	=> 'info'
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Produkty - PDF',
        'menu_title'	=> 'Produkty - PDF',
        'parent_slug'	=> 'info'
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Obchodní podmínky',
        'menu_title'	=> 'Obchodní podmínky',
        'parent_slug'	=> 'info'
    ));

}





function wpse27856_set_content_type(){
    return "text/html";
}
add_filter( 'wp_mail_content_type','wpse27856_set_content_type' );









add_action( "wp_ajax_update_session", "update_session" );
add_action( "wp_ajax_nopriv_update_session", "update_session" );


function update_session() {

    if( !session_id() ) {
        session_start();
    }


        $products_arr = $_POST['products'];

        if( count($products_arr) ) {

            $_SESSION['products'] = [];


            foreach( $products_arr as $product ) {
                if(is_numeric($product['product_qty'])) {
                    $_SESSION['products'][$product['product_id']] = [
                        'id' => $product['product_id'],
                        'qty' => $product['product_qty']
                    ];
                }
            }

        }


    // $product_qty = $_POST['product_qty'];
    // $product_id = $_POST['product_id'];

    /*
    if( isset($_SESSION['products'][$product_id]) ) {
        $_SESSION['products'][$product_id]['qty'] = $product_qty;
    }
    */

    echo 1;


    wp_die();
}











add_action( "wp_ajax_delete_session", "delete_session" );
add_action( "wp_ajax_nopriv_delete_session", "delete_session" );


function delete_session() {

    if( !session_id() ) {
        session_start();
    }



    $product_id = $_POST['product_id'];


    unset($_SESSION['products'][$product_id]);

    echo "2";

    wp_die();
}





add_action( "wp_ajax_delete_all_session", "delete_all_session" );
add_action( "wp_ajax_nopriv_delete_all_session", "delete_all_session" );


function delete_all_session() {

    if( !session_id() ) {
        session_start();
    }

    $_SESSION['cart_products'] = $_SESSION['products'];
    unset($_SESSION['products']);

    wp_die();
}











add_action( "wp_ajax_order_coupon", "order_coupon" );
add_action( "wp_ajax_nopriv_order_coupon", "order_coupon" );


function order_coupon() {

    if( !session_id() ) {
        session_start();
    }


    $product_qty = $_POST['product_qty'];
    $product_id = $_POST['product_id'];




    if(is_numeric($product_qty)){

        if( !isset( $_SESSION['products'] ) ) {
            $_SESSION['products'] = [];
        }

        if( isset($_SESSION['products'][$product_id]) ) {

            $_SESSION['products'][$product_id]['qty']  += $product_qty;

        } else {
            $_SESSION['products'][$product_id] = [
                'id' => $product_id,
                'qty' => $product_qty
            ];
        }


    }
    // $_SESSION['product_id'] = $_POST['product_id'];
    // $_SESSION['product_qty'] = $_POST['product_qty'];

    echo "1";

    wp_die();
}






function enqueue_scripts_styles_init() {


    $translation = array(
        'ajax_url' => admin_url('admin-ajax.php')
    );
    wp_register_script( 'tvin-cones-script', get_stylesheet_directory_uri() . '/js/script.js?ver=202180820', array( 'jquery' ), NULL, TRUE );
    wp_enqueue_script( 'tvin-cones-script' );
    wp_localize_script( 'tvin-cones-script', 'object', $translation );


    wp_enqueue_script( 'tvin-cones-jquery', 'http://code.jquery.com/jquery-1.11.0.min.js', array(), NULL );
    wp_enqueue_script( 'tvin-cones-jquery-migrate', 'http://code.jquery.com/jquery-migrate-1.2.1.min.js', array(), NULL );

    wp_enqueue_script( 'tvin-cones-map', get_stylesheet_directory_uri() . '/js/map.js', array(), NULL, FALSE );






    wp_enqueue_style( 'font-sans', 'https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900', array(), NULL );
    wp_enqueue_style( 'open-sans', "https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800", array(), NULL );


    wp_enqueue_style( 'tvin-cones', get_stylesheet_directory_uri() . '/css/style.css', array(), 20180508 );


}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts_styles_init' );













function getPageIDByTemplate($template)
{
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => $template
    ));

    if (!empty($pages)) {
        return $pages[0]->ID;
    }
}







function my_acf_init() {

    acf_update_setting('google_api_key', 'AIzaSyCBxsN5sYLbpWksN3zCjoZYpGipMZi1gF4');
}

add_action('acf/init', 'my_acf_init');




