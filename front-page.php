<?php get_header(); ?>
    <!-- Hlavička - začátek -->
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="main-menu-fixed">
    <div class="container container__nopad">
        <div class="menu-primary first-menu-wrap">
            <?php get_template_part('parts/category', 'menu') ?>
        </div>
    </div>
</div>
    <div class="header-text">
        <h1 class="header-h1"><span class="h1-small"><?php the_field('title_first') ?></span><br><?php the_field('title_second') ?></h1>
        <p class="header-p"><?php the_field('text') ?></p>

    </div>

    <section class="paralax-section">
        <div class="header-paralax-first"></div>
        <div class="header-paralax-second"></div>
    </section>


    <header class="header header__hp">

        <div class="shadow-center"></div>
        <div class="shadow-top"></div>

        <img class="header-center-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/header-middle.png">
    </header>



    <div class="hp-content sp-content__margin  sp-content__margin--bottom">
        <div class="container container__nopad">
            <div class="row">
                <div class="col-sm-6 left-card-wrap">

                    <div class="card card__first">
                        <div class="col-md-7 col-sm-8 col-xs-7 left-part">
                            <h2><?php if($_GET['lang']=="en"){echo "Moulded<br>cones";} else {echo "Lité<br>kornouty";} ?></h2>
                            <p><?php the_field('text_1') ?></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-5 right-part">
                            <img class="light" src="<?php echo get_stylesheet_directory_uri() ?>/images/light.png">
                            <img class="cone" src="<?php echo get_stylesheet_directory_uri() ?>/images/first-cone.png">
                            <?php if($_GET['lang']=="en"){ ?>
                                <a href="/kategorie-kornoutu/lite-kornouty/?lang=en"><button class="btn btn__orange btn__orange--card">Order</button></a>
                            <?php } else { ?>
                                <a href="/kategorie-kornoutu/lite-kornouty/"><button class="btn btn__orange btn__orange--card">Poptat</button></a>
                            <?php } ?>

                        </div>
                    </div>


                    <div class="card card__second">
                        <div class="col-md-7 col-sm-8 col-xs-7  left-part">
                            <h2><?php if($_GET['lang']=="en"){echo "Bowls<br>and cups";} else {echo "Misky<br>a poháry";} ?></h2>
                            <p><?php the_field('text_2') ?></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-5 right-part">
                            <img class="light" src="<?php echo get_stylesheet_directory_uri() ?>/images/light.png">
                            <img class="cone" src="<?php echo get_stylesheet_directory_uri() ?>/images/second-cone.png">
                            <?php if($_GET['lang']=="en"){ ?>
                                <a href="/kategorie-kornoutu/specialni-kornouty/?lang=en"><button class="btn btn__orange btn__orange--card">Order</button></a>
                            <?php } else { ?>
                                <a href="/kategorie-kornoutu/specialni-kornouty/"><button class="btn btn__orange btn__orange--card">Poptat</button></a>
                            <?php } ?>
                        </div>
                    </div>


                    <div class="card card__third">
                        <div class="col-md-7 col-sm-8 col-xs-7  left-part">
                            <h2><?php if($_GET['lang']=="en"){echo "Tubes and<br>accessories";} else {echo "Trubičky<br>a doplňky";} ?></h2>
                            <p><?php the_field('text_3') ?></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-5 right-part">
                            <img class="light" src="<?php echo get_stylesheet_directory_uri() ?>/images/light.png">
                            <img class="cone" src="<?php echo get_stylesheet_directory_uri() ?>/images/third-cone.png">
                            <?php if($_GET['lang']=="en"){ ?>
                                <a href="/kategorie-kornoutu/trubicky-a-doplnky-do-poharu/?lang=en"><button class="btn btn__orange btn__orange--card">Order</button></a>
                            <?php } else { ?>
                                <a href="/kategorie-kornoutu/trubicky-a-doplnky-do-poharu/"><button class="btn btn__orange btn__orange--card">Poptat</button></a>
                            <?php } ?>
                        </div>
                    </div>

                </div>



                <div class="col-sm-6 right-card-wrap">

                    <div class="card card__fourth">
                        <div class="col-md-7 col-sm-8 col-xs-7 left-part">
                            <h2><?php if($_GET['lang']=="en"){echo "Rounded<br>cones";} else {echo "Kroužené<br>kornouty";} ?></h2>
                            <p><?php the_field('text_4') ?></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-5 right-part">
                            <img class="light" src="<?php echo get_stylesheet_directory_uri() ?>/images/light.png">
                            <img class="cone" src="<?php echo get_stylesheet_directory_uri() ?>/images/fourth-cone.png">
                            <?php if($_GET['lang']=="en"){ ?>
                                <a href="/kategorie-kornoutu/krouzene-kornouty/?lang=en"><button class="btn btn__orange btn__orange--card">Order</button></a>
                            <?php } else { ?>
                                <a href="/kategorie-kornoutu/krouzene-kornouty/"><button class="btn btn__orange btn__orange--card">Poptat</button></a>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="card card__fifth">
                        <div class="col-md-7 col-sm-8 col-xs-7 left-part">
                            <h2><?php if($_GET['lang']=="en"){echo "Luxury<br>cones";} else {echo "Luxusní<br>kornouty";} ?></h2>
                            <p><?php the_field('text_5') ?></p>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-5 right-part">
                            <img class="light" src="<?php echo get_stylesheet_directory_uri() ?>/images/light.png">
                            <img class="cone" src="<?php echo get_stylesheet_directory_uri() ?>/images/fifth-cone.png">
                            <?php if($_GET['lang']=="en"){ ?>
                                <a href="/kategorie-kornoutu/kornouty-s-cokoladovym-okrajem/?lang=en"><button class="btn btn__orange btn__orange--card">Order</button></a>
                            <?php } else { ?>
                                <a href="/kategorie-kornoutu/kornouty-s-cokoladovym-okrajem/"><button class="btn btn__orange btn__orange--card">Poptat</button></a>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('parts/category', 'banner') ?>

<?php endwhile; ?>

    <?php get_footer(); ?>
