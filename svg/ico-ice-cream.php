<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="36px" height="59px" viewBox="0 0 36 59" style="enable-background:new 0 0 36 59;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:#F96821;}
</style>
    <g id="bg_1_" class="st0">
    </g>
    <g id="button_kontakt_kopie_2_1_">
        <g>
            <g id="Forma_1_14_">
                <g>
                    <path class="st1" d="M34.8,17.9c0.2-0.7,0.3-1.6,0.3-2.3c0-2.9-1.8-5.5-4.4-6.8C29.5,3.8,24,0,17.5,0C10.8,0,5.1,4.1,4.3,9.5
					c-1.9,1.4-3.1,3.7-3.1,6c0,0.7,0.1,1.3,0.2,1.9c-1,1.3-1.5,2.9-1.5,4.4c0,3.1,2,5.9,4.9,7L17,58.3c0.2,0.4,0.6,0.7,1,0.7
					c0.5,0,0.9-0.2,1-0.7l12.1-29.5c2.8-1.1,4.9-3.8,4.9-7C36,20.4,35.5,19,34.8,17.9z M7.6,29.9l-0.2-0.4H8L7.6,29.9z M8.5,32.3
					l3.3-3.4c0.7,0.9,1.6,1.6,2.6,2l1.7,1.7L12,36.7l-2.9-3.1L8.5,32.3z M17.7,34.2l4.1,4.2l-4.1,4.3l-4.1-4.3L17.7,34.2z
					 M11.8,40.1l0.2-0.2l4.1,4.4l-1.8,1.9L11.8,40.1z M17.9,55l-2.6-6.4l2.4-2.6l2.7,3L17.9,55z M21.4,46.6l-2.2-2.3l4.1-4.3
					l0.6,0.6L21.4,46.6z M23.3,36.7l-4.1-4.2l1.6-1.7c1.1-0.4,2-1.1,2.7-2l3.6,3.8L23.3,36.7z M28.1,30.3l-0.9-0.9
					c0.3,0,0.7,0.1,1.1,0.1h0.1L28.1,30.3z M28.3,27.2c-1.4,0-2.7-0.6-3.7-1.6c-0.2-0.2-0.7-0.3-1-0.3c-0.3,0.1-0.7,0.3-0.8,0.7
					c-0.9,1.9-2.8,3.2-5,3.2c-2,0-4-1.2-4.9-3c-0.1-0.3-0.5-0.6-0.8-0.7c-0.1,0-0.1,0-0.2,0c-0.2,0-0.6,0.1-0.8,0.3
					c-1,0.9-2.3,1.3-3.6,1.3c-2.9,0-5.4-2.3-5.4-5.3c0-1.3,0.5-2.6,1.4-3.4c0.2-0.3,0.3-0.8,0.2-1.1c-0.2-0.6-0.3-1.2-0.3-1.8
					c0-1.8,0.9-3.4,2.5-4.4c0.3-0.2,0.5-0.6,0.6-0.9c0.3-4.4,5.2-8,11.1-8c5.5,0,10.3,3.2,11,7.5c0.1,0.4,0.3,0.8,0.7,0.9
					c2.2,0.8,3.5,2.8,3.5,5c0,0.7-0.1,1.4-0.5,2.1c-0.1,0.3-0.1,0.8,0.1,1.1c0.7,0.9,1.1,2,1.1,3.2C33.6,24.8,31.2,27.2,28.3,27.2z
					 M16.5,4.1c-0.2,0-6.6,1.2-7.7,7.1c-0.1,0.6,0.2,1.2,0.9,1.3c0.1,0,0.1,0,0.2,0c0.6,0,1-0.3,1.1-0.9c1-4.3,5.8-5.2,6-5.3
					c0.6-0.1,1-0.7,0.9-1.3C17.8,4.3,17.2,4,16.5,4.1z M26.4,13.3c-0.6-0.2-1.2,0.1-1.5,0.7c-0.2,0.6,0.1,1.2,0.7,1.4
					c0.2,0.1,2.2,0.9,2,3.1c0,0.7,0.5,1.1,1.1,1.1s1.1-0.4,1.1-1.1C30,15.5,27.7,13.8,26.4,13.3z"/>
                </g>
            </g>
        </g>
    </g>
</svg>
