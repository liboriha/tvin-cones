<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="24px" height="24px" viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:#55B9DE;}
</style>
    <g id="bg_1_" class="st0">
    </g>
    <g id="button_kontakt_kopie_2_1_">
        <g>
            <g id="Forma_1_18_">
                <g>
                    <path class="st1" d="M17.3,12h-3c-0.4,0-0.7,0.3-0.7,0.8v6c0,0.4,0.3,0.8,0.7,0.8h3c0.8,0,1.5-0.7,1.5-1.5v-4.5
					C18.8,12.7,18.1,12,17.3,12z M17.3,18H15v-4.5h2.3V18z M24,13.5V12h-3.7c-0.4,0-0.8,0.3-0.8,0.8v6.7H21v-3h3V15h-3v-1.5H24z
					 M1.5,21.8V2.3c0-0.4,0.3-0.8,0.8-0.8h11.2v3C13.5,5.3,14.2,6,15,6h3v2.3h1.5v-3c0-0.2-0.1-0.4-0.2-0.5l-4.5-4.5
					C14.6,0.1,14.4,0,14.2,0h-12C1,0,0,1,0,2.3v19.5C0,23,1,24,2.3,24h5.2v-1.5H2.3C1.8,22.5,1.5,22.2,1.5,21.8z M11.2,12h-3
					c-0.4,0-0.7,0.3-0.7,0.8v6.7H9v-2.3h2.2c0.8,0,1.5-0.7,1.5-1.5v-2.2C12.8,12.7,12.1,12,11.2,12z M11.2,15.8H9v-2.2h2.2V15.8z"/>
                </g>
            </g>
        </g>
    </g>
</svg>
