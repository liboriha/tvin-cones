<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="18px" height="18px" viewBox="0 0 18 18" style="enable-background:new 0 0 18 18;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:url(#SVGID_1_);}
    .st2{fill:#FFFFFF;}
</style>
    <g id="bg_1_" class="st0">
    </g>
    <g id="button_kontakt_kopie_2_1_">
        <g id="Elipsa_1_kopie_xA0_Obraz_1_">
        </g>
        <g id="Elipsa_1_xA0_Obraz_1_">
        </g>
        <g id="Vektorový_inteligentní_objekt_xA0_Obraz_3_">
        </g>
        <g id="Vektorový_inteligentní_objekt_xA0_Obraz_1_">
        </g>
        <g id="Vektorový_inteligentní_objekt_kopie_3_xA0_Obraz_1_">
        </g>
        <g id="Vektorový_inteligentní_objekt_kopie_4_xA0_Obraz_1_">
        </g>
        <g>

            <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="10.4646" x2="18" y2="10.4646" gradientTransform="matrix(1 0 0 -1 0 19.4646)">
                <stop  offset="0" style="stop-color:#329CD0"/>
                <stop  offset="1" style="stop-color:#255EA1"/>
            </linearGradient>
            <circle class="st1" cx="9" cy="9" r="9"/>
            <g id="Zaoblený_obdélník_2_kopie_2_11_">
                <g>
                    <path class="st2" d="M12,7.7H6c-0.6,0-1,0.4-1,1s0.4,1,1,1h6c0.6,0,1-0.4,1-1S12.6,7.7,12,7.7z"/>
                </g>
            </g>
        </g>
    </g>
</svg>