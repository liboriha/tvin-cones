<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="20px" height="20px" viewBox="0 0 20 20" style="enable-background:new 0 0 20 20;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:#55B9DE;}
</style>
    <g id="bg_1_" class="st0">
    </g>
    <g id="button_kontakt_kopie_2_1_">
        <g id="Forma_1_16_">
            <g>
                <path class="st1" d="M17.7,0h-4.6c-1.3,0-2.3,1-2.3,2.3v4.6c0,1.3,1,2.3,2.3,2.3h4.6c1.3,0,2.3-1,2.3-2.3V2.3C20,1,18.9,0,17.7,0
				z M18.4,6.9c0,0.4-0.3,0.8-0.7,0.8h-4.6c-0.4,0-0.7-0.3-0.7-0.8V2.3c0-0.4,0.3-0.8,0.7-0.8h4.6c0.4,0,0.7,0.3,0.7,0.8V6.9z
				 M6.9,0H2.3C1.1,0,0,1.1,0,2.3v4.5c0,1.3,1.1,2.3,2.3,2.3h4.5c1.3,0,2.3-1.1,2.3-2.3V2.3C9.2,1.1,8.2,0,6.9,0z M7.6,6.9
				c0,0.4-0.3,0.8-0.8,0.8H2.3c-0.4,0-0.8-0.4-0.8-0.8V2.3c0-0.4,0.4-0.8,0.8-0.8h4.5c0.4,0,0.8,0.4,0.8,0.8
				C7.6,2.3,7.6,6.9,7.6,6.9z M17.7,10.8h-4.6c-1.3,0-2.3,1-2.3,2.3v4.6c0,1.3,1,2.3,2.3,2.3h4.6c1.3,0,2.3-1,2.3-2.3v-4.6
				C20,11.8,18.9,10.8,17.7,10.8z M18.4,17.7c0,0.4-0.3,0.8-0.7,0.8h-4.6c-0.4,0-0.7-0.3-0.7-0.8v-4.6c0-0.4,0.3-0.8,0.7-0.8h4.6
				c0.4,0,0.7,0.3,0.7,0.8V17.7z M6.9,10.8H2.3C1,10.8,0,11.9,0,13.1v4.5C0,18.9,1.1,20,2.3,20h4.5c1.3,0,2.3-1.1,2.3-2.3v-4.5
				C9.2,11.8,8.2,10.8,6.9,10.8z M7.6,17.7c0,0.4-0.3,0.8-0.8,0.8H2.3c-0.4,0-0.8-0.4-0.8-0.8v-4.5c0-0.4,0.4-0.8,0.8-0.8h4.5
				c0.4,0,0.8,0.4,0.8,0.8C7.6,13.2,7.6,17.7,7.6,17.7z"/>
            </g>
        </g>
    </g>
</svg>
