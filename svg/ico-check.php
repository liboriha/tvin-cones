<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="20px" height="15px" viewBox="0 0 20 15" style="enable-background:new 0 0 20 15;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:#FFFFFF;}
</style>
    <g id="Forma_1_2_" class="st0">
        <g id="Forma_1_1_">
            <g>
                <path class="st1" d="M19.6,2.1l-1.8-1.7C17.6,0.1,17.3,0,17,0s-0.6,0.1-0.9,0.4L7.7,8.6L3.9,4.9C3.6,4.7,3.3,4.6,3,4.6
				S2.4,4.7,2.1,5L0.4,6.6C0.1,6.9,0,7.2,0,7.5s0.1,0.6,0.4,0.9L5,12.9l1.8,1.7C7,14.9,7.3,15,7.7,15c0.3,0,0.6-0.1,0.9-0.4l1.8-1.7
				l9.3-9.1C19.9,3.6,20,3.3,20,2.9C20,2.6,19.9,2.3,19.6,2.1z"/>
            </g>
        </g>
    </g>
</svg>
