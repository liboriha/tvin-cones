<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="34.4px" height="49px" viewBox="0 0 34.4 49" style="enable-background:new 0 0 34.4 49;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:url(#SVGID_1_);}
</style>
    <g id="bg_1_" class="st0">
    </g>
    <g id="button_kontakt_kopie_2_1_">
        <g id="Forma_1_xA0_Obraz_1_">
            <g>
                <g>
                    <g>

                        <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="0" y1="24.9646" x2="34.3906" y2="24.9646" gradientTransform="matrix(1 0 0 -1 0 49.4646)">
                            <stop  offset="0" style="stop-color:#FF9524"/>
                            <stop  offset="1" style="stop-color:#F65220"/>
                        </linearGradient>
                        <path class="st1" d="M17.2,0C7.7,0,0,7.7,0,17.2c0,0.7,0.1,1.4,0.1,2.1C1.6,32.7,16,48.2,16,48.2c0.3,0.3,0.5,0.5,0.7,0.6l0,0
						l0.5,0.2l0.5-0.2l0,0c0.2-0.1,0.5-0.3,0.7-0.6c0,0,14.3-15.5,15.7-28.9c0.1-0.7,0.1-1.4,0.1-2.1C34.4,7.7,26.7,0,17.2,0z
						 M17.2,28.3c-6.1,0-11.1-5-11.1-11.1s5-11.1,11.1-11.1s11.1,5,11.1,11.1S23.3,28.3,17.2,28.3z"/>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
