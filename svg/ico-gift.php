<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="37.6px" height="30.5px" viewBox="0 0 37.6 30.5" style="enable-background:new 0 0 37.6 30.5;" xml:space="preserve">
<style type="text/css">
.st55{
    fill:#F65520;
}
</style>
    <g>
        <path class="st0" d="M25.6,3.8l-6.8,3.5L12,3.8 M12,3.8l6.8-3.5l6.8,3.5 M34,18.5l-11.8,5.9 M22.3,22.2v6.2 M34,16.5v6.2 M3.7,18.5
		l11.8,5.9 M15.5,22.2v6.2 M3.7,16.5v6.2 M18.8,23.8v6.2 M37.3,14.9l-18.5,9l-18.5-9 M32.4,12.1l4.8,2.4v6.6l-18.5,9l-18.5-9v-6.6
		l4.8-2.3"/>
        <line class="st1" x1="15.2" y1="5.2" x2="21.8" y2="2"/>
        <path class="st0" d="M25.7,10.7v6.8 M32.4,7.3l-6.8,3.4l-6.8-3.4 M18.9,7.3l6.8-3.4l6.8,3.4v6.8l-6.8,3.4"/>
        <line class="st1" x1="22.1" y1="8.7" x2="28.7" y2="5.5"/>
        <path class="st0" d="M12,10.7v6.8 M18.7,7.3L12,10.7L5.2,7.3 M12,17.5l-6.8-3.4V7.3L12,3.9l6.8,3.4"/>
        <polyline class="st55" points="8.3,11.6 8.3,8.8 14.8,5.5 	"/>
        <path class="st0" d="M18.8,14.2V21 M25.5,10.8l-6.7,3.4l-6.7-3.4 M12.1,10.8l6.7-3.4l6.7,3.4v6.8L18.8,21l-6.7-3.4V10.8z"/>
        <polyline class="st55" points="15.2,15.1 15.2,12.3 21.8,9 	"/>
    </g>
</svg>
