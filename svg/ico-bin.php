<svg version="1.1" id="Vrstva_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="17px" height="18px" viewBox="0 0 17 18" style="enable-background:new 0 0 17 18;" xml:space="preserve">
<style type="text/css">
    .st0{enable-background:new    ;}
    .st1{fill:#D16058;}
</style>
    <g id="bg_1_" class="st0">
    </g>
    <g id="button_kontakt_kopie_2_1_">
        <g id="Forma_1_22_">
            <g>
                <path class="st1" d="M12,6.2l-1.4-0.1l-0.3,8.3l1.4,0.1L12,6.2z M9.2,6.2H7.8v8.3h1.4V6.2z M6.8,14.5L6.5,6.2H5l0.3,8.3H6.8z
				 M12,2.7V1.2C12,0.5,11.5,0,10.8,0H6.2C5.5,0,5,0.5,5,1.2v1.5H0v1.4h1.5l1.2,13.2C2.7,17.7,3,18,3.4,18h10.2
				c0.4,0,0.7-0.3,0.7-0.6l1.2-13.2H17V2.7H12z M6.4,1.4h4.2v1.3H6.4V1.4z M12.9,16.6H4L2.9,4.1H14L12.9,16.6z"/>
            </g>
        </g>
    </g>
</svg>
