
<?php
function round_up ( $value, $precision ) {
    $pow = pow ( 10, $precision );
    return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow;
}
?>
<div class="row row__menu">
    <div class="col-lg-2 col-sm-12 logo">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php get_template_part('svg/ico', 'logo') ?></a>
    </div>



    <span onclick="menuhover()" id="mobile-menu-button"><a class="menu-icons" href="#"></a></span>



    <div class="col-lg-8 col-sm-12 menu-main">
        <?php
        wp_nav_menu( array(
            'theme_location' => 'primary',
            'depth'          => 1

        ) );
        ?>
    </div>

    <div class="col-lg-2 col-sm-12 menu-btn-wrap">
        <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-cart.php' ) ) ?>">

            <?php

            if( !session_id() ) {
                session_start();
            }

            $total = 0;
            $total_amount = 0;


            foreach ($_SESSION['products'] as $product) {
                $total += $product['qty'];
                $total_amount += $product['qty'] * get_field('price', $product['id']);
            };


            ?>


            <div class="cart-price"><?php echo round_up($total_amount, 2) ?> Kč</div>
            <div class="cart-number"><?php echo $total ?></div>
            <div class="cart-symbol btn btn__blue">
                <?php get_template_part('svg/ico', 'cart') ?>
            </div>
        </a>
    </div>
</div>

