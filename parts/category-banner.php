<section class="banner-section">
    <div class="banner-text">
        <h2 class="same-like-h1"><span class="h1-small h1-small__blue"><?php the_field('title_first', 'options') ?></span><br><?php the_field('title_second', 'options') ?></h2>
        <p class="banner-p"><?php the_field('text', 'options') ?></p>
        <a href="<?php the_permalink( getPageIDByTemplate( 'page-template/page-products.php' ) ) ?>"> <button class="btn btn__orange"><?php if($_GET['lang']=="en"){echo "All products";} else {echo "Všechny produkty";} ?></button></a>
    </div>

    <div class="footer-paralax-first"></div>
    <div class="footer-paralax-second"></div>

    <div class="hp-banner">

        <div class="shadow-center-foot"></div>
        <div class="shadow-top-foot"></div>
        <div class="container">

        </div>
        <img class="banner-center-img" src="<?php echo get_stylesheet_directory_uri() ?>/images/footer-middle.png">
    </div>
</section>
