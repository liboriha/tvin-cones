

$(document).ready(function(){
    $(window).bind('scroll',function(e){
        parallaxScroll();

    });

    function parallaxScroll(){
        var scrolledY = $(window).scrollTop();
        var height = $(document).height();
        var number = height -100;

        $('.header-paralax-first').css('top','+'+(100+(scrolledY*0.1))+'px'); //pomoci zmeny cisla menite rychlsot efektu
        $('.header-paralax-second').css('top','+'+(100+(scrolledY*0.15))+'px');  //pomoci zmeny cisla menite rychlsot efektu

        $('.footer-paralax-first').css('bottom','+'+(100+(scrolledY*0.1))+'px'); //pomoci zmeny cisla menite rychlsot efektu
        $('.footer-paralax-second').css('bottom','+'+(100+(scrolledY*0.15))+'px');  //pomoci zmeny cisla menite rychlsot efektu

        $('.footer-paralax-first').css('margin-bottom','-180px');
        $('.footer-paralax-second').css('margin-bottom','-270px');



    }

});








jQuery(document).ready(function($){

    $( "#map-display" ).focusin(function() {
        $('.h2-marker-wrap').css('display','none');
        $('.map-shaddow-top').css('display','none');
        $('.map-shaddow').css('display','none');
        $('.map-marker').css('display','none');
    });

    $( "#map" ).focusout(function() {
        $('.h2-marker-wrap').css('display','block');
        $('.map-shaddow-top').css('display','block');
        $('.map-shaddow').css('display','block');
    });

});

jQuery(document).ready(function($){
    $('.list-display').click(function() {
        $( ".product-part" ).addClass( "product__list" );
        $( ".grid-display" ).removeClass( "filter-active" );
        $( ".list-display" ).addClass( "filter-active" );
    });

    $('.grid-display').click(function() {
        $( ".product-part" ).removeClass( "product__list" );
        $( ".grid-display" ).addClass( "filter-active" );
        $( ".list-display" ).removeClass( "filter-active" );
    });
});









jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">–</div></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function() {
    var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

    btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

});





jQuery(document).ready(function($) {
    window.onscroll = function () {
        myFunction()
    };


    function myFunction() {
        if (window.pageYOffset > 1) {

            $('.main-menu-fixed').css('background-color', '#255DA0');
            $('.main-menu-fixed').css('background-image', 'linear-gradient(45deg, #255DA0 0%, #255DA0 55%, rgb(111, 201, 234) 90%)');
            $('.main-menu-fixed').css('background-position', '100% 0');
            $('.main-menu-fixed').css('background-size', '300% 200%');
            $('.menu-primary').css('margin-top', '5px');
            $('.main-menu-fixed').css("transition", "0.5s");
            $('.menu').css('padding-top', '8px');
            $('.cart-price').css('top', '24px');
            $('.menu-btn-wrap').css('padding-top', '1px');
            $('.logo svg').css('padding', '10px');
        } else {
            $('.main-menu-fixed').css('background', 'transparent');
            $('.main-menu-fixed').css("transition", "0.5s");
            $('.menu-primary').css('margin-top', '25px');
            $('.menu').css('padding-top', '12px');
            $('.cart-price').css('top', '28px');
            $('.menu-btn-wrap').css('padding-top', '3px');
            $('.logo svg').css('padding', '0px');
        }
    }

});



(function($){
    $('.btn__recalc').click(function(e) {
        e.preventDefault();
        var products = [];

        $('.quantity input').each(function(e){
            var _this = $(this);

            products.push({
                'product_id': _this.attr('data-product-id'),
                'product_qty': _this.val()
            })
        });


        var data = {
            'action': 'update_session',
            'products': products
        };

        $.post( object.ajax_url, data, function( sess_update ) {
            if( sess_update ) {

                ajax.reload();
            }
        });


    });
})(jQuery);







(function($){
    $('.btn__bin').click(function(e) {
        e.preventDefault();

        // var qty = $(this).closest('.c-number').find('.qty').val();
        var product_id = $(this).attr('data-product-id-btn');

        var data = {
            'action': 'delete_session',
            //'product_qty': qty,
            'product_id': product_id

        };
        $.post( object.ajax_url, data, function( sess_delete ) {
            if( sess_delete ) {
                location.reload();
                ajax.reload();
            }
        });

    });
})(jQuery);



(function($){


     if(window.location.pathname == '/potvrzeni-objednavky/'){
         console.log(window.location);
         var data = {
             'action': 'delete_all_session',
             //'product_qty': qty,

         };
         $.post( object.ajax_url, data, function( sess_delete ) {
             if( sess_delete ) {
                 ajax.reload();
             }
         });
     }

    if(window.location.pathname == '/order-confirmation/'){
        console.log(window.location);
        var data = {
            'action': 'delete_all_session',
            //'product_qty': qty,

        };
        $.post( object.ajax_url, data, function( sess_delete ) {
            if( sess_delete ) {
                ajax.reload();
            }
        });
    }

        // var qty = $(this).closest('.c-number').find('.qty').val();




})(jQuery);







jQuery(document).ready(function($) {
    $('.btn__add-to-cart').click(function (e) {
        e.preventDefault();


        var qty = $(this).closest('.js-pr-form').find('.js-product-qty').val();
        var product_id = $(this).attr('data-product-id');

        var data = {
            'action': 'order_coupon',
            'product_id': product_id,
            'product_qty': qty
        };


        $.post(object.ajax_url, data, function (response) {
            if (response) {
                if ($.isNumeric(qty)) {

                    $(".confirm-cart").css("opacity", "1");
                    $(".confirm-cart").css("visibility", "visible");
                    ajax.reload();
                }
                else {
                    $(".unconfirm-cart").css("opacity", "1");
                    $(".unconfirm-cart").css("visibility", "visible");
                }
            }
        });


    });

});

jQuery(document).ready(function($) {
    $('.btn__next').click(function () {
        $(".confirm-cart").css("opacity", "0");
        $(".confirm-cart").css("visibility", "hidden");
    });
    $('.btn__next').click(function () {
        $(".unconfirm-cart").css("opacity", "0");
        $(".unconfirm-cart").css("visibility", "hidden");
    });
    $('.btn__next').click(function () {
        $(".confirm-photo").css("opacity", "0");
        $(".confirm-photo").css("visibility", "hidden");
    });

    $('.btn__next').click(function () {
        $(".confirm-ingredients").css("opacity", "0");
        $(".confirm-ingredients").css("visibility", "hidden");
    });
});



jQuery(document).ready(function($) {
    $('.btn__recalc').click(function (e) {
        e.preventDefault();
        location.reload();
    });
});








var clicked = false;

function menuhover() {
    jQuery(document).ready(function($) {
        if (clicked) {
            $(".row__menu").removeClass("menu-clicked");
        }
        else {


            $(".row__menu").addClass("menu-clicked");
        }
        clicked = !clicked;
    });
}









jQuery(document).ready(function($) {
    $("a[href^='#']").click(function (e) {
        e.preventDefault();

        var position = $($(this).attr("href")).offset().top;

        $("body, html").animate({

            scrollTop: position
        } /* speed */);
    });
});


var clicked2 = false;

function CheckDelivery() {
    jQuery(document).ready(function($) {
        if (clicked2) {
            $(".del-required").prop('required', false);
            $(".delivery-wrap").removeClass("delivery-display");
        }
        else {
            $(".del-required").prop('required', true);
            $(".delivery-wrap").addClass("delivery-display");
        }
        clicked2 = !clicked2;
    });
}


var clicked3 = false;

function CheckConfirm() {
    jQuery(document).ready(function($) {
        var disabled_btn = jQuery('button[name=order_disabled]');
        var enabled_btn = jQuery('button[name=order]');
        if (clicked3) {
            $(disabled_btn).css('display', 'block');
            $(enabled_btn).css('display', 'none');
        }
        else {

            $(disabled_btn).css('display', 'none');
            $(enabled_btn).css('display', 'block');
        }
        clicked3 = !clicked3;
    });
}




jQuery(document).ready(function($) {
    $('.disabled_btn').click(function (e) {
        e.preventDefault();
        $(".confirm-cart").css("opacity", "1");
        $(".confirm-cart").css("visibility", "visible");
    });
});



jQuery(document).ready(function($) {
    $('.pr-cone').click(function (e) {
        e.preventDefault();
        $(".confirm-photo").css("opacity", "1");
        $(".confirm-photo").css("visibility", "visible");
    });
});


jQuery(document).ready(function($) {
    $('.pr-ingredients').click(function (e) {
        console.log('open-ingredients')
        console.log()

        // $( ".confirm-photo .modal-cone .lie-modal" ).html('<style>.lie-modal:before{background-image: url('+ $( this ).attr( "image" ) +') !important;}</style>');
        $( ".confirm-ingredients p" ).html('<p>'+$( this ).attr( "ingredients" )+'</p>');
        e.preventDefault();
        $(".confirm-ingredients").css("opacity", "1");
        $(".confirm-ingredients").css("visibility", "visible");
    });
});



function displayModal(index) {
    jQuery(document).ready(function($) {
            $(".display-modal-" + index).addClass("show-modal");
    });
}

jQuery(document).ready(function($) {
    $('.certificate-modal').click(function () {

        $(".display-modal").removeClass("show-modal");
    });
});