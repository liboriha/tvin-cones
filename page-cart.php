<?php /* Template name: Košík */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <div class="main-menu-fixed">
        <div class="container container__nopad">
            <div class="menu-primary">
                <?php get_template_part('parts/category', 'menu') ?>
            </div>
        </div>
    </div>


    <header class="header header__sp">
        <div class="shadow-top"></div>

        <h1 class="header-h1 header-h1__sp"><?php if($_GET['lang']=="en"){echo "Order completion";} else {echo "Dokončení objednávky";} ?></h1>
    </header>

    <div class="confirm-wrap confirm-cart">
        <div class="confirm">
            <div class="confirm-check">
                <?php get_template_part('svg/ico', 'check') ?>
            </div>
            <span class="confirm-text">
                <?php if($_GET['lang']=="en"){echo "You need to agree to the terms of business and the processing of your personal data!";} else {echo "Pro poptání zboží musíte souhlasit s obchodními podmínkami a se zpracováním osobních údajů!";} ?>

            </span>
            <br>
            <button class="btn btn__orange btn__next"><div class="next-text"><?php if($_GET['lang']=="en"){echo "Continue";} else {echo "Pokračovat";} ?></div></button>
        </div>
    </div>


    <div class="sp-content sp-content__margin">
        <div class="container">

            <form method="POST"  enctype="multipart/form-data" action="<?php the_permalink( getPageIDByTemplate( 'page-template/page-confirmation.php' ) ) ?>">
                <div class="row">
    <?php if(count($_SESSION['products']) > 0){?>
                    <div class="col-lg-6 c-col-left">
                        <div class="cart-block-1">
                            <h2><?php if($_GET['lang']=="en"){echo "Billing information";} else {echo "Fakturační údaje";} ?></h2>
                            <div class="row">
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Name";} else {echo "Jméno";} ?> *</span><br>
                                    <input required name="o_name" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your name";} else {echo "Vaše jméno";} ?>">
                                </div>
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Second name";} else {echo "Příjmení";} ?> *</span><br>
                                    <input required name="o_second_name" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your second name";} else {echo "Vaše příjmení";} ?>">
                                </div>
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Company";} else {echo "Společnost";} ?></span><br>
                                    <input type="text" name="o_company" placeholder="<?php if($_GET['lang']=="en"){echo "Name of your company";} else {echo "Jméno Vaší společnosti";} ?>">
                                </div>
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "CRN";} else {echo "IČ";} ?> *</span><br>
                                    <input required name="o_ic" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Company registration number";} else {echo "Vaše IČ";} ?>">
                                </div>
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "E-mail";} else {echo "E-mail";} ?> *</span><br>
                                    <input required name="o_email" type="email" placeholder="<?php if($_GET['lang']=="en"){echo "Your e-mail";} else {echo "Váš e-mail";} ?>">
                                </div>
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Phone";} else {echo "Telefon";} ?> *</span><br>
                                    <input required name="o_phone" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your phone number";} else {echo "Vaše telefonní číslo";} ?>">
                                </div>
                                <div class="col-sm-12">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "Street and house number";} else {echo "Ulice a č.p.";} ?> *</span><br>
                                    <input required name="o_street" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your street";} else {echo "Vaše ulice";} ?>">
                                </div>
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "City";} else {echo "Město";} ?> *</span><br>
                                    <input required name="o_city" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your city";} else {echo "Vaše město";} ?>">
                                </div>
                                <div class="col-sm-6">
                                    <span class="form-span"><?php if($_GET['lang']=="en"){echo "ZIP";} else {echo "PSČ";} ?> *</span><br>
                                    <input required name="o_psc" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your postal code";} else {echo "Vaše PSČ";} ?>">
                                </div>


                            </div>
                        </div>

                        <div class="cart-block-2">
                            <div class="row delivery-wrap">

                                <div class="col-sm-6 col-xs-12">
                                    <h2><?php if($_GET['lang']=="en"){echo "Delivery information";} else {echo "Doručovací údaje";} ?></h2>
                                </div>

                                <div class="col-sm-1 col-xs-1">
                                    <input class="option-input checkbox checkbox__first" onclick="CheckDelivery()" name="o_delivery_info" type="checkbox">
                                </div>
                                <div class="col-sm-5 col-xs-11">
                                    <p class="c-p"><?php if($_GET['lang']=="en"){echo "Are the delivery informations different?";} else {echo "Jsou doručovací údaje jiné?";} ?></p>
                                </div>



                                <div class="delivery-info">
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "Name";} else {echo "Jméno";} ?> *</span><br>
                                        <input class="del-required" name="o_del_name" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your name";} else {echo "Vaše jméno";} ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "Second name";} else {echo "Příjmení";} ?> *</span><br>
                                        <input class="del-required" name="o_del_second_name" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your second name";} else {echo "Vaše příjmení";} ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "Company";} else {echo "Společnost";} ?></span><br>
                                        <input type="text" name="o_del_company" placeholder="<?php if($_GET['lang']=="en"){echo "Name of your company";} else {echo "Jméno Vaší společnosti";} ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "CRN";} else {echo "IČ";} ?> *</span><br>
                                        <input class="del-required" name="o_del_ic" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Company registration number";} else {echo "Vaše IČ";} ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "E-mail";} else {echo "E-mail";} ?></span><br>
                                        <input type="email" name="o_del_email" placeholder="<?php if($_GET['lang']=="en"){echo "Your e-mail";} else {echo "Váš e-mail";} ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "Phone";} else {echo "Telefon";} ?> *</span><br>
                                        <input class="del-required" name="o_del_phone" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your phone number";} else {echo "Vaše telefonní číslo";} ?>">
                                    </div>
                                    <div class="col-sm-12">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "Street and house number";} else {echo "Ulice a č.p.";} ?> *</span><br>
                                        <input  class="del-required" name="o_del_street" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your street";} else {echo "Vaše ulice";} ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "City";} else {echo "Město";} ?> *</span><br>
                                        <input  class="del-required" name="o_del_city" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your city";} else {echo "Vaše město";} ?>">
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="form-span"><?php if($_GET['lang']=="en"){echo "ZIP";} else {echo "PSČ";} ?> *</span><br>
                                        <input  class="del-required" name="o_del_psc" type="text" placeholder="<?php if($_GET['lang']=="en"){echo "Your postal code";} else {echo "Vaše PSČ";} ?>">
                                    </div>
                                </div>



                            </div>
                            <span class="form-span"><?php if($_GET['lang']=="en"){echo "Notes";} else {echo "Poznámky";} ?></span><br>
                            <textarea class="c-textarea" name="o_comment" placeholder="<?php if($_GET['lang']=="en"){echo "Your text";} else {echo "Váš text";} ?>"></textarea>

                            <div class="row">
                                <div class="col-xs-1">
                                    <input class="option-input checkbox checkbox__second" name="confirm_order" onclick="CheckConfirm()" type="checkbox">
                                </div>
                                <div class="col-xs-11">
                                    <p class="c-p">

                                        <?php if($_GET['lang']=="en"){
                                            echo 'I agree with the <a style="text-decoration:underline" target="_blank" href="' . get_field('obchodni_podminky', 'options') . '">terms and conditions</a> and I take note of the processing of my personal data.';
                                        } else {
                                            echo 'Souhlasím s <a style="text-decoration:underline" target="_blank" href="' . get_field('obchodni_podminky', 'options') . '">obchodními podmínkami</a> a beru na vědomí zpracování osobních údajů.';
                                        } ?>
                                    </p>
                                </div>
                            </div>


                        </div>
                    </div>
    <?php } ?>

                    <div class="col-lg-6  c-col-right">
                        <div class="cart-block-3">
                            <div class="c-product-h2">
                                <h2><?php if($_GET['lang']=="en"){echo "Products";} else {echo "Produkty";} ?></h2>
                            </div>
                            <div class="c-product-wrap">

            <?php

            if( !session_id() ) {
                session_start();
            }

            if($_SESSION['products']):




            foreach ($_SESSION['products'] as $product) {
                $product_qty = $product['qty'];
                $product_id = $product['id'];
                $product_price = $product_qty * get_field('price',"$product_id");
                $price_dph = get_field('price_dph',"$product_id");


            ?>
                                <div class="c-product">

                                        <div class="c-product-head">
                                            <div class="c-cone">
                                                <img class="c-lie" src="<?php the_field("img","$product_id")?>">
                                            </div>
                                            <div class="c-title-wrap">
                                                <div class="c-title">
                                                    <?php the_field("title","$product_id")?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="c-number">
                                            <div class="quantity">
                                                <input min="1" class="qty" data-product-id="<?php echo $product_id ?>"  value="<?php echo $product_qty; ?>" type="number">
                                            </div>
                                            <span class="c-unit">Ka</span>
                                        </div>

                                        <div class="c-price">
                                            <?php if(get_field('price',"$product_id") != 'Na dotaz'){?>
                                                <?php echo Number_Format(($product_price),2,"."," ");?> Kč

                                                <span class="without_dph without_dph--cart"><?php $price = $product_price; $result = $price * $price_dph;?>Cena s DPH: <?php echo $price + round_up($result, 2); ?> Kč </span>
                                            <?php }else{ ?>
                                                Na dotaz
                                            <?php } ?>
                                        </div>

                                        <div class="c-bin">
                                            <a class="btn__bin" data-product-id-btn="<?php echo $product_id ?>"  href=""><?php get_template_part('svg/ico', 'bin') ?></a>
                                        </div>

                                </div>
                <?php
                };

                else:
                ?>
                <p><?php if($_GET['lang']=="en"){echo "Your Cart is currently empty!";} else {echo "Váš košík je momentálně prázdný!";} ?></p>
    <?php endif; ?>

                            </div>


                            <?php

                            if( !session_id() ) {
                                session_start();
                            }



                            $total_amount_cart = 0;
                            $total_amount_cart_dph = 0;

                            foreach ($_SESSION['products'] as $product_cart) {
                                $total_amount_cart += $product_cart['qty'] * get_field('price', $product_cart['id']);
                                $total_amount_cart_dph += $product_cart['qty'] * get_field('price', $product_cart['id']) * get_field('price_dph', $product_cart['id']);
                            };

                            ?>

                            <div class="total-amount-block">
                                <?php if($_SESSION['products']){ ?>
                                    <div class="button-recalc-wrap"><button class="btn btn__blue btn__recalc"><?php if($_GET['lang']=="en"){echo "Convert";} else {echo "Přepočítat";} ?></button></div>
                                <?php } ?>
                                <div class="total-amount">
                                    <?php if($_GET['lang']=="en"){echo "Total price without shipping:";} else {echo "Celková cena bez dopravy:";} ?>
                                    <br>
                                    <span style="    font-size: 13px;
    line-height: normal;
    font-weight: normal;
    display: block;
    margin-top: 15px;">Zboží rozvážíme pouze ve Středočeském kraji a v Praze.</span>
                                </div>
                            <div class="total-price"><?php echo round_up($total_amount_cart, 2) ?> Kč
                                    <span class="without_dph without_dph--cart-main">DPH: <?php $price = round_up($total_amount_cart, 2); $result = $total_amount_cart_dph; echo round_up($result, 2); ?> Kč | Cena: <?php echo $price + round_up($result, 2); ?> Kč </span>
                                </div>
                            </div>


                        </div>





                        <?php if(count($_SESSION['products']) > 0){?>

                        <div class="cart-block-4">
                            <h2><?php if($_GET['lang']=="en"){echo "Payment pickup";} else {echo "Výběr platby";} ?></h2>

                            <?php
                            $display = false;
                            if($display == true){
                                ?>
                            <div class="row row__pay">
                                <div class="col-sm-1">
                                    <input class="option-input radio" value="invoice"  name="o_payment" type="radio">
                                </div>
                                <div class="col-sm-8">
                                    <span class="c-pay-choice"><?php if($_GET['lang']=="en"){echo "Invoice with payment within 14 days";} else {echo "Faktura s platbou do 14 dnů";} ?></span><br>
                                    <p class="c-p"><?php if($_GET['lang']=="en"){echo "Payment is only available to registered partners and more detailed information";} else {echo "Platba je dostupná pouze pro registrované partnery a další podrobnější informace";} ?></p>
                                </div>
                                <div class="col-sm-3">
                                    <span class="c-pay-price"><?php if($_GET['lang']=="en"){echo "Free";} else {echo "Zdarma";} ?></span>
                                </div>
                            </div>
                            <?php } ?>


                            <div class="row row__pay">
                                <div class="col-sm-1">
                                    <input class="option-input radio" value="cash" checked name="o_payment" type="radio">
                                </div>
                                <div class="col-sm-8">
                                    <span class="c-pay-choice"><?php if($_GET['lang']=="en"){echo "Payment in cash upon takeover";} else {echo "Platba v hotovosti při převzetí";} ?></span><br>
                                    <p class="c-p"></p>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <span class="c-pay-price"><?php if($_GET['lang']=="en"){echo "Free";} else {echo "Zdarma";} ?></span>
                                </div>
                            </div>

                                <button name="order" style="display:none"  id="cart_button" class="btn btn__orange btn__cart btn__cart-function"><?php if($_GET['lang']=="en"){echo "Order";} else {echo "Poptat";} ?></button>
                                <button name="order_disabled" style="display:block" id="cart_button" class="btn btn__orange btn__cart disabled_btn"><?php if($_GET['lang']=="en"){echo "Order";} else {echo "Poptat";} ?></button>


                        </div>
                        <?php } ?>
                    </div>

                </div>

            </form>

        </div>
    </div>

<?php endwhile; ?>
<?php get_footer(); ?>